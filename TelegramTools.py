from lib.core import *
from lib.TelethonClient import *



class TelegramToolsUI(TelegramTools):

	@log.catch()
	def __init__(self):
		super().__init__()
	
	@log.catch()
	def __del__(self):
		super().__del__()
		
	@log.catch()
	def RunAction(self, action='', title=''):

		try:
			method = getattr(self,action)

			if method:
				
				printTitle(action if not title else title)

				asyncio.run(method())

		except KeyboardInterrupt:
			print("User Abort.")

		except Exception as e:
			log.error(e)
			
		input(ye+"\n [?] Press Enter to go to the main menu...")


	@log.catch()
	async def CheckSessions(self, spambot=False):

		spambot = bool(strtobool(input(" [?] Check with SpamBot? (y/n): ") or 'n'))

		sessions = self.SessionsDataAsDict()
		ss = []
		if sessions:
			for s in sessions.keys():
				if int(sessions[s]['last_check_time']) < time.time() - 5*MINUTE_IN_SECONDS :
					ss.append(s)

			if len(ss)>0:
				tsk = []
				print(gr+f"\n Preparing to check {len(ss)} accounts{' using SpamBot' if spambot else ''}, that have not been checked in the last 5 minutes.")
				threads = self.Config().getint('Common','max_threads',fallback=5)
				i = 0
				for x in range(len(ss)):
					if i < threads: 
						print(ye+f" [{ss[x]}] Starting ...")
						tsk.append(ss[x])
						i+=1
					if i >= threads or x == len(ss)-1:
						if len(tsk)>0:
							log.debug(f"Starting {len(tsk)} threads using asyncio.")
							await asyncio.gather(*[self._check_session(spambot=spambot,session=s) for s in tsk])
							log.debug(f"Finished executing '{len(tsk)}' Threads using asyncio.")
						await asyncio.sleep(0)
						i=0
						tsk.clear()

	@log.catch()
	async def _check_session(self,session,spambot):
		
		await async_wait(random.randrange(5,15))

		cl = TelethonClient(session,display_profile=False, clean=True)

		if await cl.ConnectClient():
			print(gr+f" [{session}] OK.")
		else :
			print(re+f" [{session}] Failed.")

		await async_wait(random.randrange(5,15))
		
		return

	@log.catch()
	async def _connect_and_disconnect(self,client):
		await async_wait(random.randrange(5,15))
		await client.Disconnect()
		return

	@log.catch()
	async def OpenWebClient(self):

		session = self.SelectClient()

		print("Opening browser. Please be patient...")
		self.Popen(['OpenWebClient.py', '--session', session],
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE,
				restart=True,
				sys_executable=True)

	@log.catch()
	async def CreateJsonMetadata(self):

		clear()
		select = []
		sessions = self.ListSessions(True)

		if len(sessions) > 0:
			i = 0
			for s in sessions:
				if os.path.isfile(self.Path(DIR['TELETHON_SESSION'],s+".json")): continue
				print(gr+f" [{i}] {s}")		
				select.append(s)
				i+=1

			if len(select)>0:
				print(re+"\n ==== Select sessions/accounts to create json metadata for: \n")
				s_sel = multi_select_from_list(select)
				# print(re+" Press Ctrl+C to to abort...")
				# s_select = input(ye+ "\n [?] Choose which sessions you want to use (use a comma to seperate, 'all' to use all sessions (exclude option next)): ").lower().split(',')
				# s_exclude = s_sel = []

				# if 'all' in s_select:
				# 	s_exclude = input(ye+" [?] Select the sessions you would like to exclude (optional, comma seperated): ").lower().split(',')
				# 	s_sel = [select[x] for x in range(len(select)) if x not in [int(y.strip()) for y in s_exclude if y]]
				# else:
				# 	s_sel = [select[int(x.strip())] for x in s_select if int(x.strip()) <= len(select)]

				if len(s_sel)>0:
					limit = len(s_sel)
					_max = len(s_sel) if len(s_sel) <= limit else limit
					tasks = []
					for i in range(_max):
						cl = TelethonClient(s_sel[i],display_profile=False)
						if await cl.ConnectClient():
							print(ye+f" - starting {s_sel[i]}...")
							tasks.append(cl)
					if len(tasks)>0:
						await asyncio.gather(*[ self._connect_and_disconnect(c) for c in tasks])
					# await asyncio.sleep(0)

					print(gr+" === All done")

			else:
				print(re+" No sessions found with missing metadata. You are good to go.")
	
	@log.catch()
	async def SimpleTelegramClient(self):
		try:
			while True:

				printHeading("Simple Telegram Client")

				session = self.SelectClient()
				client = TelethonClient(session)

				if not await client.ConnectClient():
					return

				print(ye+"\n Notes:")
				print(ye+" - For 'brand new' Telegram accounts, open a chat dialog with SpamBot for instructions on getting set up:")

				print(re+"\nGetting most recent chats...\n")
				# Print all dialog IDs and the title, nicely formatted
				chat = []
				i = 0
				for dialog in await client.GetDialogs(limit=200, archived=False):
					is_channel = True if str(dialog.id).startswith("-100") else False
					# if not str(dialog.id).startswith("-100"):
					chat.append(dialog)
					# print(dialog)
					print(gr+' [{}] [{}] {}'.format(i,"📢 " if is_channel else "👤 ", dialog.title), (f"({dialog.unread_count})" if dialog.unread_count > 0 else ""))	
					i+=1
					# if i > 10:
					# 	break

				print(gr+" \n\n>>>>> [x] = Exit, [r] = Refresh")
				print(gr+" \n>>>>> [s] = Open Dialog with SpamBot, [u] = Update Profile ")
				open_chat = input(ye+"\n [?] Chat / Action: ").lower()

				if open_chat == 'x':
					return
				if open_chat == 'r':
					continue
				if open_chat == 's':
					spambot = await client.raw.get_input_entity("SpamBot")
					msg = await client.raw.send_message(spambot, "/start")
					await asyncio.sleep(2)
					continue
				if open_chat == 'u':
					# await update_profile(self.Client)
					continue

				if open_chat.isdigit() and int(open_chat) > len(chat)-1:
					print(re+" [!] Wrong choice! Try again.")
					await asyncio.sleep(2)
					continue
				elif not open_chat.isdigit():
					print(re+" [!] Wrong choice! Try again.")
					await asyncio.sleep(2)
					continue

				while True:
					clear()

					peer = await client.raw.get_entity(chat[int(open_chat)].id)

					# From oldest to most-recent
					for message in reversed( await client.raw.get_messages(peer,  limit=100)):
						if not message:
							continue
						clr = gr if message.from_id and hasattr(message.from_id, "user_id") and message.from_id.user_id == await self.Client.get_peer_id("me") else ye
						name = "me" if message.from_id and hasattr(message.from_id, "user_id") and message.from_id.user_id == await self.Client.get_peer_id("me") else chat[int(open_chat)].title
						print(clr+f'[{name}] [{message.date}]', message.text)
						# print(message.id, message.text)	
						# print(message.stringify())		

					o = 0
					# print(message.stringify())
					if message.reply_markup:
						print(re+" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
						print(gr+" >>>>> Button Options:")
						btns = []
						if hasattr(message.reply_markup, 'rows'):
							for button in message.reply_markup.rows:
								if hasattr(button, 'buttons'):
									for btn in button.buttons:
										print(re+f'   [{o}] '+btn.text)
										btns.append(btn.text)
										o+=1
										# print(message.reply_markup.stringify())

					print(re+" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
					# print(peer)
					if hasattr(peer,'username') and peer.username == 'SpamBot':
						print(gr+"\nNOTES on SpamBot and Spam Status:")
						print(ye+" - To check if your account is limited, it is easier to use the Client Status Tool ")
						print(ye+" - If this is a 'brand new' Telegram account, you need to follow these steps to message non-contacts:")
						print(ye+" > > > If no Button Options are available, Send message '/start'.")
						print(ye+" > > > Choose Button Option 'But I can’t message non-contacts!' .")
						print(ye+" > > > Choose Button Option 'No, I’ll never do any of this!' .")
						print(ye+" > > > Write any reason, such as 'Im new. I didnt know I was blocked.' .")
						print(ye+" > > > Your account restrictions will be lifted within a couple of hours.")
						print("\n\n")

					print(gr+" >>>>> [x] = Exit, [b] = Back, [r] = Refresh, [m] = Mark as read, [a] = Archive chat ")
					print(re+" >>>>> Anything else sends a message!! ")
					usr_action = input(ye+"\n [?] Action / Message Text: ")

					try:
						if usr_action.lower() == 'b':
							break
						if usr_action.lower() == 'x':
							return
						if usr_action.lower() == 'm':
							async for message in client.raw.iter_messages(peer, reverse=True, limit=15):
								message.mark_read()
							break
						if usr_action.lower() == 'a':
							await client.raw.edit_folder(peer,1)
							break
						if usr_action.lower() == 'r':
							continue
						if o > 0:
							try:
								val = int(usr_action)
								if val < o:
									msg = await client.raw.send_message(peer, btns[val])
									continue
							except ValueError:
								pass

						# Anything else entered will be sent as a message
						if len(usr_action) > 0:
							# new_message = input(" [?] Type Message: ")
							msg = await client.raw.send_message(peer, usr_action)
							continue
					except Exception as e:
						log.error(e)
						await async_wait(2)
						continue


		except Exception as e:
			log.error(e)

	@log.catch()
	async def ViewLogs(self):
		rr = int(input(ye+"\n [?] Set a refresh rate? (in seconds): ") or 1)

		while True:
			clear()
			print("""
				┏┓
				┃┃
				┃┃╋╋┏━━┳━━┳━━┓
				┃┃╋┏┫┏┓┃┏┓┃━━┫
				┃┗━┛┃┗┛┃┗┛┣━━┃
				┗━━━┻━━┻━┓┣━━┛
				╋╋╋╋╋╋╋┏━┛┃
				╋╋╋╋╋╋╋┗━━┛
				""")
			print(gr+"\nChoose Log file\n"+ye)
			logs = []
			try:
				i = 1
				for f in os.listdir(DIR['LOGS']):
					if not f.endswith(".log") or not f:
						continue
					print(gr+f" [{i}] {f[:f.rfind('.')]}")
					logs.append(f)
					i+=1
			except Exception as e:
				log.error(e)
			v = input(ye+"\n [?] Choose Log file (x=Abort): ")
			if not v.isnumeric():
				break
			ft = str(input(ye+" [?] Filter for text (matches all substrings seperated by a space, any order, case-insensitive): ") or '').split(" ")

			if os.path.isfile(self.Path(DIR['LOGS'],logs[int(v)-1])):
				try:
					while True:
						clear()
						if ft:
							out = [l for l in open(self.Path(DIR['LOGS'],logs[int(v)-1]),encoding='utf-8') if all(x.lower() in l.lower() for x in ft)]
						else:
							out = [l for l in open(self.Path(DIR['LOGS'],logs[int(v)-1]),encoding='utf-8')]
						
						for l in out[-100:]:
							if "error" in l.lower(): c = re
							elif "debug" in l.lower(): c = ye
							else: c = gr
							print(c + l.strip("\n"))

						print(re+"\n\n Refreshing... Press Ctrl+C to Abort..."+ye)
						time.sleep(rr)
				except KeyboardInterrupt:
					pass

	@log.catch()
	async def AddRemoveRoles(self):
		sessions = self.SelectClient(handle_selection=False)

		print(re+"\n Press Ctrl+C to to abort...")
		s_select = input(ye+ " [?] Select which sessions you want to work with (use a comma to seperate, 'all' to use all sessions (exclude option next)): ").lower().split(',')
		s_exclude = s_sel = []

		if 'all' in s_select:
			s_exclude = input(ye+" [?] Select the sessions you would like to EXCLUDE (optional, comma seperated): ").lower().split(',')
			s_sel = [sessions[x] for x in range(len(sessions)) if x not in [int(y.strip()) for y in s_exclude if y]]
		else:
			s_sel = [sessions[int(x.strip())] for x in s_select if int(x.strip()) <= len(sessions)]		
		if len(s_sel)>0:

			while True:
				action = input(ye+" [?] What would you like to do? (add/remove) ").lower().strip()
				if not any( [action == a for a in ['add','remove']]): continue
				break

			role = input(ye+" [?] Name of role: ").lower().strip()

			for s in s_sel:
				md = self.GetSessionMetadata(s).get('telegram-tools').get('role')
				if action == 'add' and not role in md: 
					md.append(role)
					self.UpdateSessionMetadata(s,{'telegram-tools':{'role': md}})
				elif action == 'remove' and role in md:
					md.remove(role)
					self.UpdateSessionMetadata(s,{'telegram-tools':{'role': md}})

			print(gr+" === DONE")

	@log.catch()
	async def ViewSessions(self):
		# show database
		app.SelectClient(handle_selection=False)

	@log.catch()
	async def ViewGroupData(self):
		session = self.SelectClient()
		if session:
			client = TelethonClient(session)
			if await client.ConnectClient():
				print(re+" Loading chats ....")
				dialogs = await client.GetDialogs(ignore_auto_save=True)
				if dialogs:
					print("\n\n")
					h = ' ID '
					h = h + sp(15-len(h)) + 'Type'
					h = h + sp(25-len(h)) + 'Username'
					h = h + sp(60-len(h)) + 'Users'
					h = h + sp(70-len(h)) + 'Name'
					print(re+h)
					for d in dialogs:
						if isinstance(d.entity,types.Channel):
							_type = "Channel" if d.entity.broadcast else "Group"
							p = f' {d.entity.id}'
							p = p + sp(15-len(p)) +f"{_type}"
							p = p + sp(25-len(p)) +f"{'@'+d.entity.username if d.entity.username else ''}"
							p = p + sp(60-len(p)) +f"{d.entity.participants_count}"
							p = p + sp(70-len(p)) +f"{d.entity.title}"
							print((gr if _type=='Group' else ye) + p)
							# print(gr+f" [{_type}] ({d.entity.id}) {d.entity.title} ({d.entity.participants_count} users) [@{d.entity.username}]")
					# print(d)
					input(ye+"\n [?] Press Enter to continue...")					

	@log.catch()
	async def GetSessionString(self):
		session = self.SelectClient()
		if session:
			client = TelethonClient(session,session_string='new',display_profile=False)
			client2 = TelethonClient(session)
			if await client2.ConnectClient():
				if await client.ConnectClient(auto_confirmation=client2):
					ss = await client.SessionString()
					print(f"Your session string is:\n\n`{ss}`\n\nKeep this secret! This was sent to your saved messages too.")				
					await client.SendMessage('me', f"Your session string is:\n\n{ss}\n\nKeep this secret!")
			
	@log.catch()
	async def MassUpdateMetadata(self):
		while True:
			sessions = self.SelectClient(handle_selection=False)
			print(re+"\n Press Ctrl+C to to abort...")
			s_select = input(ye+ " [?] Select which sessions you want to work with (use a comma to seperate, 'all' to use all sessions (exclude option next)): ").lower().split(',')
			s_exclude = s_sel = []

			if 'all' in s_select:
				s_exclude = input(ye+" [?] Select the sessions you would like to EXCLUDE (optional, comma seperated): ").lower().split(',')
				s_sel = [sessions[x] for x in range(len(sessions)) if x not in [int(y.strip()) for y in s_exclude if y]]
			else:
				s_sel = [sessions[int(x.strip())] for x in s_select if int(x.strip()) <= len(sessions)]		
			if len(s_sel)>0:
				print(re+"\nMetadata Sample")
				print(ye+"\n KEY"+sp(50-11)+"VALUE")
				mdsample = self.GetSessionMetadata(s_sel[0])
				for k in mdsample.keys(): 
					if isinstance(mdsample[k], int) or isinstance(mdsample[k], str) or isinstance(mdsample[k], dict):
						p = re+f" {k} "
						if not isinstance(mdsample[k], dict):
							p = p + sp(50-len(p)) + gr + str(mdsample[k])
							print(p)
						else:
							# print(p)
							for kk in mdsample[k].keys():
								if isinstance(mdsample[k][kk], int) or isinstance(mdsample[k][kk], str):
									p = re+f" {k}:{kk} "
									p = p + sp(50-len(p)) + gr+ str(mdsample[k][kk])
									print(p)

				key = input("\n [?] Change Metadata for which KEY (parent:child) : ").split(":")
				value = input(" [?] Change Metadata VALUE for chosen key: ")
				vtype = input(" [?] What type of VALUE should it be (boolean,string,integer): ").lower()

				if key and value:
					if 'int' in vtype: value = int(value)
					elif 'bool' in vtype: value = bool(strtobool(value))
					else: value = str(value)
					new = {}
					for s in s_sel:
						md = self.GetSessionMetadata(s)
						if len(key)>1:
							if key[0] in md:
								new[key[0]] = {}
								if key[1] in md[key[0]]:
									# md[key[0]][key[1]] = value
									new[key[0]][key[1]] = value
						else:
							new[key[0]] = value
						self.UpdateSessionMetadata(s,new)

	@log.catch()
	async def AddSingleSession(self):
		while True:
			_session = input(ye+" [?] What is your phone number? ")
			if _session == '' or _session.lower() == 'x':
				break
			client = TelethonClient(_session, phone=_session)
			if await client.ConnectClient():
				print(gr+" Client Saved")
			else :
				print(re+" Client failed")

	@log.catch()
	async def OtherTools(self):

		# await self.ConnectClient()

		while True:
			printTitle("Other Tools")
			print(" Actions:")

			l = [
				'Open Simple Chat Terminal',	# 0
				'Verify YAML configuration',	# 1

			]
			for a in l:
				print(gr+f" [{l.index(a)}] {a}")

			ac = input(ye+"\n [?] Choose action (x = abort): ")

			if ac == 'x':
				break
			elif ac == '0':
				await self.SimpleTelegramClient()
			elif ac == '1':
				await self.VerifyFile()

			else:
				continue

if __name__ == "__main__":

	app = TelegramToolsUI()

	while True:
		clear()
		
		print(re+banner+"\n")

		printHeading("Client/Session Manager (lite)")

		menu = [
			{'name':'Open Web Client','function':'OpenWebClient','options':[]},
			{'name':'Client/Session Management','function':'','options':[
				{'name':'Auto session bulk-creation tool','function':''},
				{'name':'Import Single Account','function':'AddSingleSession'},
				{'name':'Auto session bulk-creation tool (background)','function':'BulkAddSession'},
				{'name':'Configure sessions','function':''},
				{'name':'Resolve Spambot','function':'ResolveSpamBot'},
				{'name':'Check Sessions','function':'CheckSessions'},
				{'name':'Create Missing Metadata','function':'CreateJsonMetadata'},
				{'name':'Add/Remove Roles (tags)','function':'AddRemoveRoles'},
				{'name':'List Channel Data (Id,Username,participants,etc)','function':'ViewGroupData'},
				{'name':'Mass Update JSON Metadata','function':'MassUpdateMetadata'},
				{'name':'Get SessionString for user','function':'GetSessionString'},

			]},
			{'name':' - - - - ','function':'','options':[]},
			{'name':'Other Tools','function':'OtherTools','options':[]},
			{'name':'View Sessions','function':'ViewSessions','options':[]},
			{'name':'View Log files','function':'ViewLogs','options':[]},

		]

		for m in menu: print(gr+f" [{menu.index(m)}] {m['name']} ")

		print(gr+"\n\n - - - - - - ")
		print(gr+" [X] - Exit")

		print(ye+"\n\n Notes: ")
		print(ye+"\t - 'Client' or 'Session' refers to logged into Telegram Account")
		print(ye+"\t - To use emojis in text, use this format: https://www.webfx.com/tools/emoji-cheat-sheet/")

		tool = input(ye+"\n [?] Which tool would you like to use: ").lower()
		execute = None

		if tool == 'x':
			print("\nGoodbye...\n")
			# time.sleep(1)
			break
		elif tool.isnumeric():
			try:
				menu[int(tool)]
				if len(menu[int(tool)]['options'])>0:
					printTitle(menu[int(tool)]['name'])
					for a in menu[int(tool)]['options']: print(gr+f" [{menu[int(tool)]['options'].index(a)}] {a['name']}")
					ac = input(ye+"\n [?] Choose action (x = abort): ").strip()
					if ac == 'x':
						continue
					elif ac.isnumeric():
						try: 
							menu[int(tool)]['options'][int(ac)]['function']
							execute = (menu[int(tool)]['options'][int(ac)]['function'], menu[int(tool)]['options'][int(ac)]['name'])
						except: pass
				if not execute and menu[int(tool)]['function']:
					execute = (menu[int(tool)]['function'],menu[int(tool)]['name'])
				if execute:
					app.RunAction(execute[0],execute[1])
			except Exception as e:
				log.error(e)	

	sys.exit(0)
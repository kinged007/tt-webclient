from lib.core import *

from telethon.sync import TelegramClient
from telethon import functions, types, errors, sessions
from datetime import datetime, timedelta

class TelethonClient(TelegramTools):

	def __init__(self, session, **kwargs):

		""" Main class to interact with telethon api. """

		# super().__init__()

		# clean session name
		if any(session.startswith(x) for x in ['+','.']):
			session = session[1:]

		# options
		self._display_profile = True
		self._clean = False # TODO use Config() 
		self._autosave = False # TODO use Config()
		self._session_string = None
		self._phone = None

		if kwargs:
			for k in kwargs.keys():
				if k == 'display_profile' : self._display_profile = bool(kwargs[k])
				if k == 'clean' : self._clean = bool(kwargs[k])
				if k == 'autosave' : self._autosave = bool(kwargs[k])
				if k == 'session_string' : self._session_string = str(kwargs[k])
				if k == 'phone' : self._phone = str(kwargs[k])
				

		log.debug(f"TelethonClient working with session '{session}'.")

		self.raw = None # stores the raw TelegramClient object

		self.metadata = self.GetSessionMetadata(session) # prepares metadata with default values
		if self._phone:
			self.metadata['phone'] = self._phone

		self.me = None
		self.session = None
		# self.raw = self.raw
		self.twofa = None
		self._is_connected = False
		self._is_locked = False

		self._connect_failures = 0
		self._session = session
		
		return

	@log.catch()
	def __del__(self):
		asyncio.ensure_future(self.Disconnect())

	"""
		
		HELPERS

	"""


	@log.catch()
	def IsLocked(self,session=''):

		if not self._session and not session:
			log.error("No session name is loaded.")
			return True

		if session:
			if os.path.exists(self.Path(DIR['TELETHON_SESSION'],session+".session-journal")): return True
			return False

		if os.path.exists(self.Path(DIR['TELETHON_SESSION'],self._session+".session-journal")):
			self._is_locked = True
			return True
		self._is_locked = False
		return False



	@log.catch()
	async def IsConnected(self):
		await asyncio.sleep(0)
		return self._is_connected
		# return True
		# try:
		# 	# log.debug("Checking if client is connected.")
		# 	if self.Client:
		# 		# log.debug("Checking if client is connected 2.")
		# 		if await self.Client.get_me():
		# 			# log.debug("Checking if client is connected - Is Connected.")
		# 			return True
		# except:
		# 	pass
		
		# return False

	@log.catch()
	async def Disconnect(self):

		log.debug("Disconnecting Telethon client")
		try:
			await self.raw.disconnect()
		except:
			pass

		if self.session:
			self.UpdateSessionMetadata(self.session,{'telegram-tools':{'locked':False}})
			# self.metadata['telegram-tools']['locked'] = False
			# self.UpdateSessionMetadata(self.session,self.metadata)
		
		self.me = None
		self.session = None
		# self.raw = self.raw
		self.twofa = None
		self._is_connected = False
		self._is_locked = False
		
		log.debug("Disconnected!")


	@log.catch()
	def ConnectionFailure(self):
		self._connect_failures += 1
		if self._connect_failures >= self.Config().getint('Common','change_proxy_after_failed_attempts',fallback=5):
			if self.metadata['proxy']:
				log.warning("Too many connection attempts. Changing proxy server.")
				with open(DIR['PROXIES']+'/inactive.txt','a', encoding='utf-8') as f:
					f.write(self.metadata['proxy'][1]+"\n")
				self.metadata = self.GetProxy()
				self._connect_failures = 0
				if self.metadata['proxy'] and self.raw:
					self.raw.set_proxy(self.PrepareProxyForTelethon(self.metadata['proxy']))
					return False

	@log.catch()
	async def _populate_metadata(self,metadata,me):

		# loops through metadata and fills in gaps using me object.
		# only parsing first level of keys, related to me
		if not metadata or not me:
			return metadata

		# print(metadata)
		# print(me)

		# protect against unsupported64bit data and messing up our json file
		_skip = False
		try:
			if 'unsupporteduser64' in me.username.lower():
				log.error("Telegram responded with UnsupportedUser64 bit data. Update telethon.")
				_skip = True
		except:
			pass

		for k in metadata.keys():
			if not _skip:
				if k =='first_name' and metadata[k] != me.first_name: metadata[k] = me.first_name
				if k =='last_name' and me.last_name and metadata[k] != me.last_name: metadata[k] = me.last_name
				if k =='phone' and metadata[k] != me.phone: metadata[k] = me.phone
				if k =='username' and me.username and metadata[k] != me.username: metadata[k] = me.username
			if k =='register_time' and not metadata[k]: metadata[k] = int(time.time())
			if k =='session_file' and metadata[k] != self.session: metadata[k] = self.session

		return metadata

	"""
		
		TELETHON SESSIONS

	"""
	@log.catch()
	async def SessionString(self):
		return self.raw.session.save()
		if await self.IsConnected():
			return self.raw.session.save()

	@log.catch()
	async def ConnectClient(self, auto_confirmation:TelegramClient = None):
		"""
			auto_confirmation used to get login confirmation code automatically

		"""
		new = sms = skip_proxy = uses_2fa = remove_unauthorised = quick = False
		_authorising = _skip = False

		# using session string or not
		_session = None
		if self._session_string: 
			log.debug("Connecting to Telethon using StringSession")
			_session = sessions.StringSession(self._session_string) if "=" in self._session_string else sessions.StringSession()
			_skip = True
		if not _session:
			log.debug("Connecting to Telethon using Session database")
			_session = self.Path(DIR['TELETHON_SESSION'],self._session)


		if not self._session:
			log.error("No session to connect to.")
			return False 

		# if not os.path.exists(self.Path(DIR['TELETHON_SESSION'],self._session+".session")):
		# 	if self._phone:
		# 		# we have a phone number to use
		# 		pass
		# 	if not _skip:
		# 		log.error("Session database is missing.")
		# 		return False
		if not self.metadata:
			log.error("Session metadata is missing.")
			return False

		if self._is_connected:
			log.warning("Client is already connected.")
			return False

		if self.IsLocked() and not _skip:
			log.warning("Client is locked. Probably in use by another process. Waiting 30 seconds.")
			await async_wait(30)
			if self.IsLocked():
				log.warning("Client is locked. Probably in use by another process. Aborting.")
				return False

		if not self.metadata['app_id'] and not self.metadata['app_hash']:
			api = self.GetRandomApi()
			self.metadata['app_id'] = api[0]
			self.metadata['app_hash'] = api[1]
		if not self.metadata['app_id'] or not self.metadata['app_hash']:
			log.error("API credentials are missing! Please add them to the config file.")
			return False

		proxy = None
		if not self.metadata['proxy'] and self.Config().getboolean('Common','use_proxy',fallback=False) is True:
			self.metadata['proxy'] = self.GetProxy()
			if self.metadata['proxy']:
				proxy = self.PrepareProxyForTelethon(self.metadata['proxy'])

		if self.metadata['proxy'] and self.Config().getboolean('Common','use_proxy',fallback=False) is True:
			# format proxy for telegramclient
			proxy = self.PrepareProxyForTelethon(self.metadata['proxy'])

		device = self.GetRandomDevice()
		self.metadata['device'] = self.metadata['device'] if self.metadata['device'] else device[0]
		self.metadata['sdk'] = self.metadata['sdk'] if self.metadata['sdk'] else device[1]
		self.metadata['app_version'] = self.metadata['app_version'] if self.metadata['app_version'] else device[2]

		if not self.metadata['system_lang_pack']:
			self.metadata['system_lang_pack'] = self.GetRandomLangCode()

		# set up TelegramClient
		# TODO https://docs.telethon.dev/en/stable/modules/client.html#telethon.client.telegramclient.TelegramClient
		# TelegramClient(session: typing.Union[str, Session], api_id: int, api_hash: str, 
		#		proxy: Union[tuple, dict] = None, timeout: int = 10, device_model: str = None, system_version: str = None, app_version: str = None, 
		#		lang_code: str = 'en', system_lang_code: str = 'en')
		try:
			if self.Config().getboolean('Common','use_proxy',fallback=False) and proxy:
				log.info("Connecting to TelegramClient with proxy: "+str(proxy)+", and API ID|Hash: "+str(self.metadata['app_id'])+" | "+str(self.metadata['app_hash']))
			else:
				log.info("Connecting to TelegramClient without proxy, and API ID|Hash: "+str(self.metadata['app_id'])+" | "+str(self.metadata['app_hash']))

			self.raw = TelegramClient(
				_session, 
				self.metadata['app_id'], 
				self.metadata['app_hash'], 
				timeout=60, 
				proxy = proxy,
				device_model = self.metadata['device'],
				system_version = self.metadata['sdk'],
				app_version = self.metadata['app_version'],
				lang_code = self.metadata['system_lang_pack'],
				system_lang_code = self.metadata['system_lang_pack']
			)
		
		except Exception as e:
			log.error(e)
			return False

		# lets try to connect
		a = 5
		while a>0:
			try:
				# await making_api_call()
				if await self.raw.connect():
					break
				a-=1
			except Exception as e:
				log.error(e)
				if 'failed' in str(e).lower():
					self.ConnectionFailure()

		# check that we are authorised. if not, do something
		a = 5
		try:
			# await making_api_call()
			while not await self.raw.is_user_authorized():
				if self._clean:
					log.info("Auto-remove Unauthorized Sessions is on. Removing Unauthorized session.")
					await self.Disconnect()
					await async_wait(3)
					self.DeleteSession(self._session)
					return False
				_authorising = True
				print(f"\nAttempting to authorise session '{self._session}' ...(Ctrl+C aborts the operation.)\n")
				# according to rules: You must provide a phone and a code the first time, and a password only if an RPCError was raised before.
				try:
					if self.metadata['phone'] :
						await self.raw.send_code_request(self.metadata['phone'], force_sms=True)
					else:
						self.metadata['phone'] = input(ye+" [?] What is your phone number (International format): ")
						await self.raw.send_code_request(self.metadata['phone'], force_sms=True)

					if auto_confirmation:
						code = await auto_confirmation.GetConfirmationCode()
						log.debug(f"Received confirmation code: {code}")
						await self.raw.sign_in(self.metadata['phone'], code=code)							
					else:
						await self.raw.sign_in(self.metadata['phone'], code=input(ye+" [?] Enter code received: "))
				except errors.PhoneNumberUnoccupiedError:
					# await making_api_call()
					await self.raw.sign_up(code, first_name=input(ye+" [?] Enter a First Name: ") )
				except errors.SessionPasswordNeededError:
					log.debug("2FA Password is required...")
					if self.metadata['twoFA']:
						# await making_api_call()
						await self.raw.sign_in(self.metadata['phone'], password=self.metadata['twoFA'])
					else:
						self.metadata['twoFA'] = input(ye+" [?] Enter your 2FA password: ")
						await self.raw.sign_in(self.metadata['phone'], password=self.metadata['twoFA'])
					_authorising = False # do not try to set a 2fa password, because we already have one
				except errors.PhoneNumberBannedError:
					log.error("Phone number has been banned.")
					self.DeleteSession(self._session)
					return False
				except Exception as e:
					log.error(e)	

				a-=1
				await async_wait(15)
				if a <= 0:
					log.error("Could not authorise session. Deleting session.")
					self.DeleteSession(self._session)
					return False
		except KeyboardInterrupt:
			print(re+"\nUser aborted.")
			# TODO ask if want to delete session file
			if input(ye+" [?] Delete Session? (y/n) ").lower() == 'y':
				self.DeleteSession(self._session)
			return False		
		except Exception as e:
			log.error(e)	
			return False	


		# Now we should be connected

		# get me
		me = None
		try:
			# await making_api_call()
			me = await self.raw.get_me()
		except Exception as e:
			log.error(e)
			return False

		if me:
			# obviously we are connected

			# TODO Check/Set 2FA
			# do it manually! 
			# https://telethonn.readthedocs.io/en/latest/extra/basic/creating-a-client.html#two-factor-authorization-2fa
			if self.Config().getboolean('Common','enable2fa',fallback=False) and _authorising:
				if not self.metadata['twoFA']:
					twofa = self.Config().get('Common','password2fa',fallback='')
					if not twofa or '[random]' in twofa.lower():
						twofa = self.GeneratePassword()
					self.metadata['twoFA'] = twofa

				try:
					if self.metadata['twoFA'] :
						await self.raw.edit_2fa(new_password=self.metadata['twoFA'])
						log.info(f"Successfully set up 2FA for client '{self._session}' with password '{self.metadata['twoFA']}'.")
				except Exception as e:
					log.error("Setting 2FA password failed.")
					log.error(e)
					self.metadata['twoFA'] = None # dont save the password because it did not save in TG

			# Set up variables
			# self.Session.clear()
			self.me = me
			self.session = self._session
			# self.raw = self.raw
			self.twofa = self.metadata['twoFA']
			self._is_connected = True
			self._is_locked = True
			self.metadata['last_check_time'] = int(time.time())
			self.metadata['telegram-tools']['user_id'] = int(me.id)
			self.metadata['telegram-tools']['locked'] = True

			# poopulate metadata with me
			self.metadata = await self._populate_metadata(self.metadata,self.me)

			self.UpdateSessionMetadata(self.session,self.metadata)
			log.debug(self.metadata)
			if self.metadata['register_time']:
				age = str((int(time.time()) - int(self.metadata['register_time']))/DAY_IN_SECONDS)
				log.debug(f"Account '{self.session}' registered on {time.strftime('%x %X',time.localtime(int(self.metadata['register_time'])))} is {age[:age.rfind('.')+3]} days old. ")
			
			if self.Config().getboolean('Common','auto_save_api_credentials',fallback=False):
				if not str(self.metadata['app_id']) in self.Config()['my.telegram.org']:
					# log.warning(f"TODO API '{self.metadata['app_id']}' needs to be saved in config.ini.")
					pass

			# display Logged in user
			if self._display_profile:
				full = await self.raw(functions.users.GetFullUserRequest("me"))
				print("")
				print(re+"Logged in as",me.first_name )
				print(gr+"\n\n======= User Profile =======")
				print(" [First Name] "+str(me.first_name))
				print(" [Last Name] "+str(me.last_name))
				print(" [Username] "+str(me.username))
				print(" [Phone] +"+str(me.phone))
				print(" [About] "+str(full.about))
				print(" [Restricted] "+str(me.restricted))
				print(" [Scam] "+str(me.scam))
				print("===============================")
				print("")
				await async_wait(2)

			# finished
			return self.raw		

		return None

	@log.catch()
	async def Me(self):
		try:
			return self.me
		except:
			pass
		return None


	@log.catch()
	async def GetConfirmationCode(self):

		error_count = 10
		while True:
			try:
				login_code = ''
				tid = None
				await async_wait(5)
				await making_api_call()
				dialogs = await self.GetDialogs(ignore_auto_save=True)
				for d in dialogs:
					# print(f" [{d.id}] {d.title}")
					if d.title == 'Telegram':
						# print("JACKPOT!")
						tid = d.id

				if tid:
					for message in await self.raw.get_messages(tid,10):
						# log.debug(message.date)
						# log.debug(message.date + timedelta(minutes=10))
						# log.debug(datetime.now(message.date.tzinfo))

						if not message or ':' not in message.text:
							continue
						try:
							import re as regex
							log.debug(message.text)
							# log.debug(regex.serch(r"(\d{1,6})",message,text).group(1))
							login_code = regex.search(r"(\d{1,6})",message.text).group(1)
							# login_code = message.text[message.text.lower().rfind(":")+1:message.text.find(".")].replace("*","").strip()
							if login_code:
								# print(login_code)
								return login_code
							break
						except:
							pass
							# if login_code:
							# 	# print(login_code)
							# 	return login_code
							# break

				break
			except Exception as e:
				log.error(e)
				error_count-=1
				if 'connection' in str(e).lower():
					self.ConnectionFailure()
				if error_count <= 0:
					log.error("Too many failed attempts.")
					break

		return None

	@log.catch()
	async def GetEntity(self, v):
		return await self.raw.get_entity(v)

	@log.catch()
	async def GetDialogs(self,limit=500, archived=False,ignore_auto_save=False):
		try:
			await making_api_call()
			self.dialogs = await self.raw.get_dialogs(limit=limit, archived=archived)
			if not ignore_auto_save: await self.AutoSaveGroups(self.dialogs)
			return self.dialogs
		except Exception as e:
			log.error(e)
		return []

	@log.catch()
	async def AutoSaveGroups(self,dialog):
		if self.Config().getboolean('Common','auto_save_groups',fallback=False):
			log.warning("Auto saving groups is activated. Will take a little longer to load while auto-saving.")
			# if isinstance(dialog,object):
			# 	dialog = [dialog]
			if not os.path.exists(self.Path(DIR['PARTICIPANTS'],'auto_saved')):
				os.mkdir(self.Path(DIR['PARTICIPANTS'],'auto_saved'))
			start = time.time()
			if isinstance(dialog,list):
				try:
					cols = ['id','title','username']
					exists = []
					extract_users = self.Config().getboolean('Common','auto_export_users_from_groups',fallback=False)
					extract_admin = self.Config().getboolean('Common','auto_export_admin_from_groups',fallback=False)
					if os.path.exists(self.Path(DIR['GROUPS'],"auto_saved.csv")):
						with open(self.Path(DIR['GROUPS'],"auto_saved.csv"), 'r',encoding='utf-8') as f:
							reader = csv.reader(f,delimiter=",",lineterminator="\n")
							exists = [x[0] for x in reader]
					with open(self.Path(DIR['GROUPS'],"auto_saved.csv"), 'a',encoding='utf-8') as f:
						writer = csv.writer(f,delimiter=",",lineterminator="\n")
						if len(exists)==0:
							writer.writerow(cols)
						for d in dialog:
							# print(int(time.time()-start))
							if (extract_users or extract_admin) and time.time()-start < 30:
								if not os.path.exists(self.Path(DIR['PARTICIPANTS'],'auto_saved',f"non_admin_{d.entity.id}_{cleanString(d.title)}.csv")) or not os.path.exists(self.Path(DIR['PARTICIPANTS'],'auto_saved',f"admin_{d.entity.id}_{cleanString(d.title)}.csv")):
									try:
										await self.raw.get_participants(d.id,limit=1)
										if extract_users:
											await self.GetParticipants(d.id,audience_type = 'users',audience_filter='all',save_path=self.Path(DIR['PARTICIPANTS'],'auto_saved',f"non_admin_{d.entity.id}_{cleanString(d.title)}.csv"))
										if extract_admin:
											await self.GetParticipants(d.id,audience_type = 'admin',audience_filter='all',save_path=self.Path(DIR['PARTICIPANTS'],'auto_saved',f"admin_{d.entity.id}_{cleanString(d.title)}.csv"))
									except:
										pass

							if str(d.id) in exists: continue
							if not str(d.id).startswith("-100"): continue
							# TODO If username is empty, try get an invite link?
							writer.writerow([d.id,d.entity.title,d.entity.username])
					
				except Exception as e:
					log.error(e)


	@log.catch()
	async def ImportSessionsAsContacts(self,_max=4):

		""" TODO :: NOT TESTED!! """

		await making_api_call()

		try:
			me = self.me

			sessions = self.SessionsDataAsDict()

			await making_api_call()
			_contacts = await self.raw(functions.contacts.GetContactsRequest(hash=0))
			contacts = [int(c.user_id) for c in _contacts.contacts]
			add_contact = []
		except Exception as e:
			log.error(e)
			return

		try:	
			for row in sessions:
				if _max <= 0:
					break
				# checking for self, check if contact already added
				if row['telegram-tools']['user_id'] == me.id or int(row['telegram-tools']['user_id']) in contacts:
					continue

				log.info(f"Importing session ID: {row['first_name']} {row['last_name']}...")

				# add new contact
				add_contact.append(types.InputPhoneContact(
						client_id=random.randrange(-2**63, 2**63),
						phone=str(row['phone']),
						first_name=str(row['first_name']),
						last_name=str(row['last_name'])
					))
				_max-=1
		except Exception as e:
			log.error(e)
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)

		if len(add_contact)>0:
			try:
				if SIMULATION:
					log.info("Skipping importing of contacts in Simulation mode.")
					pass

				await making_api_call()
				result = await self.raw(functions.contacts.ImportContactsRequest(
					contacts=add_contact
				))

				# print(result)

			except Exception as e:
				log.error(e)			

		log.info(f"Finished importing {len(add_contact)} contacts. ")

	@log.catch()
	async def SendMessage(self, peer, text):
		try:
			self._SendMessage_counter
		except:
			self._SendMessage_counter = 0

		self._SendMessage_counter += 1
		if self._SendMessage_counter > 6: # after 6 attempts
			self.IncrementSessionFloodError(self.session)
			return False

		# TODO check target peer. Channel/User/Group


		# print(peer)

		# continue...
					


		if ":" in text:
			# probably using emojis
			text = emoji.emojize(text, use_aliases=True)


		try:
			await making_api_call()
			if not SIMULATION:
				entity = await self.GetEntity(peer)
				msg = await self.raw.send_message(entity, text)
				await async_wait(5)
				if self.Config().getboolean('Common','archive_message_after_send',fallback=True):
					await self.raw.edit_folder(entity,1) # move message to archived folder
				log.info(f"Send SUCCESS: user '{entity.id}' with message '"+text[0:50].replace("\n","")+"'")
				return True
			else:
				log.debug("Simulating Sending in DEBUG mode.")
				b = random.randrange(0,10)
				if b <= 3:
					log.error(f"SIMULAION: Send FAILED: user '{entity.id}' with message '"+text[0:50].replace("\n","")+"'")
					return False
				log.info(f"SIMULAION: Send SUCCESS: user '{entity.id}' with message '"+text[0:50].replace("\n","")+"'")
				return True

		except Exception as e:
			log.error(e)
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)
			if 'too many requests' in str(e).lower():
				if self._SendMessage_counter > 5:
					# too many requests. unlikely going to change.
					log.error("Too many failed attempts. Consider deleting session.") 
					# await self.ResolveSpamBot()
					return "Too many failed attempts. Consider deleting session."
				await async_wait(random.randrange(150,600))
				return await self.SendMessage(entity,text) # trying again
		except errors.PeerFloodError as e:
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)

			log.error("PeerFloodError: Please try again after some time (a day maybe to be safe). Waiting 24-30 Hours...")
			return "PeerFloodError: Please try again after some time (a day maybe to be safe). Waiting 24-30 Hours..."
			# await async_wait(random.randrange(24,30)*HOUR_IN_SECONDS)
			# return await self.SendMessage(entity,text)

		except errors.FloodWaitError as e:
			log.error(re+f'FloodWaitError: Waiting for {e.seconds} secs')
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)
			await async_wait(e.seconds+120)
			return await self.SendMessage(entity,text) # trying again

		log.error(f"Send FAILED: user '{entity.id}' with message '"+text[0:50].replace("\n","")+"'")
		return False

	@log.catch()
	async def ResolveSpamBot(self):
		try:
			await self.ConnectClient()
			spambot = await self.raw.get_input_entity("SpamBot")
			print(ye+" - @SpamBot: /start")
			await self.raw.send_message(spambot, "/start")
			await asyncio.sleep(random.randrange(3,8))	
			for m in await self.raw.get_messages(spambot): #client.iter_messages(spambot, reverse=True, limit=2):
				if "/start" in m.text:
					continue
				print(gr+f"[{m.date}] {m.text}")
				break					
			print(ye+" - @SpamBot: But I can’t message non-contacts!")
			await self.raw.send_message(spambot, "But I can’t message non-contacts!")
			await asyncio.sleep(random.randrange(3,9))	
			for m in await self.raw.get_messages(spambot): #client.iter_messages(spambot, reverse=True, limit=2):
				if "/start" in m.text:
					continue
				if 'already' in me.text:
					return
				print(gr+f"[{m.date}] {m.text}")
				break					
			print(ye+" - @SpamBot: No, I’ll never do any of this!")
			await self.raw.send_message(spambot, "No, I’ll never do any of this!")
			await asyncio.sleep(random.randrange(2,6))	
			for m in await self.raw.get_messages(spambot): #client.iter_messages(spambot, reverse=True, limit=2):
				if "/start" in m.text:
					continue
				print(gr+f"[{m.date}] {m.text}")
				break					
			print(ye+" - @SpamBot: Im new. I didnt know I was blocked. Please unblock me.")
			await asyncio.sleep(random.randrange(6,15))
			await self.raw.send_message(spambot, "Im new. I didnt know I was blocked. Please unblock me.")
			await asyncio.sleep(random.randrange(2,5))
			for m in await self.raw.get_messages(spambot): #client.iter_messages(spambot, reverse=True, limit=2):
				if "/start" in m.text:
					continue
				print(gr+f"[{m.date}] {m.text}")
				break					
			
			print(gr+"\nNow, Wait a 1-2 hours before trying to send messages to non-contacts.")

		except Exception as e:
			print(re+"Error:",e)		

	@log.catch()
	async def GetGroupsList(self, limit=9999):
		
		chats = []
		channel = []

		try:
			result = await self.raw(functions.messages.GetDialogsRequest(
				offset_date=None,
				offset_id=0,
				offset_peer=types.InputPeerEmpty(),
				limit=limit,
				hash=0
				))
			chats.extend(result.chats)
			for a in chats:
				try:
					participants = await self.raw.get_participants(a,limit=1)
						
					channel.append(a)
				except:
					continue
		except Exception as e: 
			log.error(e)
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)

		return channel

	@log.catch()
	async def JoinGroup(self, id, username=''): 
		try:	

			await making_api_call()
			entity = await self.raw.get_entity(int(id))

			await making_api_call()
			if await self.raw(functions.channels.JoinChannelRequest(entity)):
				# print(gr+f"Joined {group.title}")
				return True

		except errors.FloodWaitError as e:
			log.warning(f'Flood Error. Waiting for {e}')
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)
			await async_wait(e.seconds+25)
			return await self.JoinGroup(id)
		except Exception as e:
			log.warning(e)
			if "Could not find the input entity" in str(e):
				try:
					if 'http' in username:
						await making_api_call()
						updates = await self.raw(functions.messages.ImportChatInviteRequest(username[username.rfind('/')+1:len(username)]))
						log.debug(updates)
						return True

					elif username:
						await making_api_call()
						entity = await self.raw.get_entity(username)
						await making_api_call()
						updates = await self.raw(functions.channels.JoinChannelRequest(entity))
						log.debug(updates)
						if updates:
							# print(gr+f"Joined {group.title}")
							return True					
					else:
						log.warning(f"Could not join group {id}")
				
				except Exception as e:
					log.error(e)

		return False

	@log.catch()
	async def LeaveGroup(self,id):

		try:	

			await making_api_call()
			if await self.raw.delete_dialog(int(id)):
				# print(gr+f"Joined {group.title}")
				return True

		except errors.FloodWaitError as e:
			log.warning(f'Flood Error. Waiting for {e}')
			if 'Flood' in str(e):
				self.IncrementSessionFloodError(self.session)
			await async_wait(e.seconds+25)
			return await self.LeaveGroup(id)
		except Exception as e:
			log.error(e)

		return False		

	@log.catch()
	async def CheckSpamStatus(self):

		try:
			if not await self.IsConnected():
				await self.ConnectClient()
			
			spambot = await self.raw.get_input_entity("SpamBot")
			msg = await self.raw.send_message(spambot, "/start")
		except Exception as e:
			# print(e)
			return False

		await asyncio.sleep(2)
		# req = await self.raw(functions.messages.StartBotRequest("SpamBot","me","start"))
		for message in await self.raw.get_messages(spambot): #client.iter_messages(spambot, reverse=True, limit=2):
			if "/start" in message.text:
				continue
			if "no limits" in message.text:
				return True
		return False



	@log.catch()
	async def GetParticipants(self,entity, audience_type = 'users',audience_filter='recent', save_path = ''):

		result = []
		skip_admins = []
		users = []

		if not entity:
			log.error("Invalid entity.")
			return result

		log.debug(f"GetParticipants with audience_type={audience_type}, audience_filter={audience_filter}.")
		try:
			await making_api_call()
			_entity = await self.raw.get_entity(entity)
			if not isinstance(_entity,types.Channel): 
				log.warning("Entity is not a Channel: "+str(type(_entity)))
				return result
			log.debug("Entity: "+str(_entity))
			_filter = None
			if audience_type == 'admin':
				_filter = types.ChannelParticipantsAdmins()
			if audience_filter == 'recent':
				_filter = types.ChannelParticipantsRecent()
			if audience_filter not in ['active']:
				# await making_api_call()
				# async for x in self.raw.iter_participants(_entity,filter=_filter,aggressive=False):
				# 	users.append(x)
				users = await self.raw.get_participants(_entity, filter=_filter )
				log.debug(f"Extracted {len(users)} of a total of {users.total} users.")
		except Exception as e:
			log.error(e)
			if 'Flood' in str(e):
				# self.metadata['telegram-tools']['FloodErrors'] = int(self.metadata['telegram-tools']['FloodErrors']) + 1 if 'FloodErrors' in self.metadata['telegram-tools'] else 1
				self.UpdateSessionMetadata(self.session, {'telegram-tools':{'FloodErrors':int(self.metadata['telegram-tools']['FloodErrors']) + 1}})
			return result

		_entity_id = None
		try:
			_entity_id = _entity.channel_id
		except:
			try:
				_entity_id = _entity.id
			except:
				log.error("Could not find the id for entity: "+str(_entity))

		try:
			if audience_filter == 'active':
				# get active users
				# TODO iter through messages to find users (authors)
				# users = []
				pass

			if audience_type == 'users':
				# filter out admin
				await making_api_call()
				admin = await self.raw.get_participants(_entity, filter=types.ChannelParticipantsAdmins() )
				log.debug(f"Removing {len(admin)} admins from list of participants.")
				# remove admin users from user list.
				skip_admins = [u.id for u in admin]


		except Exception as e:
			log.error(e)


		if users:
			write = []
			for user in users:
				if (user.bot | user.deleted | user.restricted | user.scam | user.is_self) or user.id in skip_admins:
					continue	
				result.append(user)
				write.append([
					user.id,
					user.access_hash,
					user.username if user.username else '',
					user.first_name, 
					user.last_name if user.last_name else '',
					user.phone,
					_entity_id, 
					_entity.title,
				])		
			if save_path and save_path.endswith(".csv"):
				try:
					with open(save_path,"w",encoding='UTF-8') as f:
						writer = csv.writer(f,delimiter=",",lineterminator="\n")
						writer.writerow(['id','access_hash','username','first_name','last_name','phone','group_id','group_title'])
						writer.writerows(write)
				except Exception as e:
					log.error(e)

		return result



	"""
		
		DATABASE

	"""
	
	@log.catch()
	def DeleteSession(self,session,path='deprecated'):
		# Deletes .session file and removes from DB
		if SIMULATION:
			log.info(f"Skipping Deleting of Session '{session}' in Simulation mode.")
			return True
		try:
			frm = DIR['TELETHON_SESSION']
			if os.path.isfile(self.Path(frm,session + ".session")):
				shutil.move(self.Path(frm,session+".session"),self.Path(DIR['TELETHON_SESSION_ARCHIVE'],session+".session"))
			if os.path.isfile(self.Path(frm,session + ".json")):
				shutil.move(self.Path(frm,session+".json"),self.Path(DIR['TELETHON_SESSION_ARCHIVE'],session+".json"))
				# os.remove(file)
		except Exception as e: 
			log.error(e)

		#deactivate session in jobs
		try:
			for j in self.ListProjects():
				pa = self.Path(DIR["SUBPROCESSES"],j);
				if os.path.isfile(self.Path(pa,"config.ini")):
					# print(f" ==== {j} ====")
					cfg = configparser.ConfigParser(allow_no_value=True)
					cfg.read(self.Path(pa,"config.ini"))
					if cfg.getboolean('Common','active',fallback=False):
						# active job. find runners
						for s in cfg.sections():
							if s == session: 
								# found our session. set var
								cfg[s]['active'] = False
								with open(self.Path(pa,"config.ini"), 'w', encoding='utf-8') as configfile:
									cfg.write(configfile)
		except Exception as e: 
			log.error(e)


	"""
		
		???

	"""
""" Helper functions """
try:
	import os, configparser, sys,  asyncio, time, random, json
	from loguru import logger as log
	from sys import platform
	from pathlib import Path
	from pprint import pprint
	from distutils.util import strtobool
	from filelock import Timeout, FileLock

except Exception as e:
	log.error(e)
	raise

if platform=='win32':
	clear = lambda:os.system('cls')
else:
	clear = lambda:os.system('clear')

# TODO see https://pypi.org/project/colorama/
# or https://github.com/Textualize/rich
COLORS = {
	"re": "\u001b[31;1m",
	"gr": "\u001b[32m",
	"ye": "\u001b[33;1m",
}
locals().update(COLORS)

def colorText(text):
	for color in COLORS:
		text = text.replace("[[" + color + "]]", COLORS[color])
	return text


######################## CONFIG AND GLOBALS #########################

MINUTE_IN_SECONDS = 60
HOUR_IN_SECONDS = MINUTE_IN_SECONDS*60
DAY_IN_SECONDS = HOUR_IN_SECONDS*24
WEEK_IN_SECONDS = DAY_IN_SECONDS*7
MONTH_IN_SECONDS = DAY_IN_SECONDS*30

__basename = str(Path(os.path.dirname(os.path.realpath(__file__))).parent.absolute())
CONFIG = configparser.ConfigParser(allow_no_value=True, delimiters=('='))
try:
	CONFIG.read(os.path.join(__basename,'config.ini'))
	CONFIG['Common']['debug_mode']
except:
	if os.path.exists(os.path.join(__basename,'config.ini')):
		os.remove(os.path.join(__basename,'config.ini'))
	exit("ERROR loading configuration file.")


######################## DEBUG AND LOG #########################

DEBUG = CONFIG.getboolean('Common','debug_mode',fallback=False)
SIMULATION = False
NOWAIT = False

# pd.set_option("chained_assignment", None)

# Overides
if len(sys.argv) > 0:
	for a in sys.argv:
		if a.lower() == '--debug':
			DEBUG = True
		if a.lower() == '--simulation':
			SIMULATION = True
			NOWAIT = True
		if a.lower() == '--no-wait':
			NOWAIT = True

#############################################################################

@log.catch()
def printTitle(title):
	clear()
	print("\n")
	print(ye+"= "*20)
	print(re+" "+str(title))
	print(ye+"= "*20)
	print("\n")

@log.catch()
def printHeading(title):
	print("\n")
	print(ye+"- "*20)
	print(re+" "+str(title))
	print(ye+"- "*20)
	print("\n")

@log.catch()
def printTable(data:list,header:list=[],col_width:list=[]):
	""" eg: printTable(header=[' #','Type','username','Users','Name'], col_width=[5,10,35,10,30], data=tbl_df)
			tbl_df.append([
				(gr if _type=='Group' else ye) + str(i), 
				_type, 
				'@'+d.entity.username if d.entity.username else '',
				d.entity.participants_count,
				d.entity.title
			])
	"""
	# TODO
	pass

@log.catch()
def multi_select_from_list(lst:list,question:str=''):
	q = question if question else "Make your selection: "
	print(re+"\n Press Ctrl+C to to abort...")
	print(ye+" Note: use a comma to seperate multiple selections; 'all' to select all options. If 'all' is selected, the next question will allow you to exclude some options.\n")
	s_select = input(ye+ f" [?] {q} ").lower().split(',')
	s_exclude = s_sel = []

	if 'all' in s_select:
		s_exclude = input(ye+" [?] (optional) EXCLUDE options: ").lower().split(',')
		s_sel = [lst[x] for x in range(len(lst)) if x not in [int(y.strip()) for y in s_exclude if y]]
	else:
		s_sel = [lst[int(x.strip())] for x in s_select if int(x.strip()) <= len(lst)]	

	return s_sel

@log.catch()
def make_path(path):
	""" makes directories for given path """
	try:
		from pathlib import PurePath
		parts = PurePath(path).parts
		pth = ''
		for x in parts:
			if '.' in x or os.path.isfile(os.path.join(pth,x)): break
			pth = os.path.join(pth,x)
			if not os.path.exists(pth): os.mkdir(pth)
		return True
	except Exception as e:
		log.error(e)
	return False

@log.catch()
def ensure_path_exists(path):
	return make_path(path);

@log.catch()
async def making_api_call(t=0):

	making_api_call.counter += 1

	if not hasattr(making_api_call, "last_call"):
		making_api_call.last_call = time.time()
		log.opt(depth=2,record=True).debug("API CALL "+str(making_api_call.counter))
		return

	log.opt(depth=2,record=True).debug("API CALL "+str(making_api_call.counter) + ": {:n}".format(time.time() - making_api_call.last_call) +" seconds since last call") #{time.strftime('%H:%M:%S', time.localtime())}")

	if time.time() - making_api_call.last_call < 5:
		t = random.randrange(2,5)+t
		log.opt(depth=2).debug(f"Waiting for {t} seconds.")
		await asyncio.sleep(t)

	making_api_call.last_call = time.time()

making_api_call.counter = 0

@log.catch()
async def async_wait(t=0):
	if not t:
		t = random.randrange(10,30)
	if NOWAIT:
		t = random.randrange(3,6)
	log.opt(depth=2).debug(f"Waiting for {t} seconds.")
	await asyncio.sleep(t)

@log.catch()
def wait(t):
	if not t:
		t = random.randrange(10,30)
	log.opt(depth=2).debug(f"Waiting for {t} seconds.")
	if NOWAIT:
		t = random.randrange(3,6)
	time.sleep(t)

""" spacer text """
@log.catch()
def sp(length=1):
	o = " "*(int(length))
	return o

""" Clean text from ascii characters and emojis """
def cleanString(inputString):
    return replace_all(inputString.encode('ascii', 'ignore').decode('ascii'), {
    	'"':'',
    	"'":'',
    	"/":'_',
    	"\\" : '_',
    	"@" : "_at_",
    	
    	})

""" replace string with search:value dict """
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


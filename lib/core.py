#########################################################
#########################################################
###################	  Telegram Tools  ###################
################# Client Management #####################
#########################################################

import os, sys, traceback, time, subprocess, shutil, csv
from sys import platform
from pathlib import Path


# virtualenv support
is_venv = (hasattr(sys, 'real_prefix') or (hasattr(sys, 'base_prefix') and sys.base_prefix != sys.prefix))

def _telegram_tools_install():

	if platform=='win32':
		os.system('cls')
	else:
		os.system('clear')	

	print("""

	╭━━╮╱╱╱╱╱╭╮╱╱╱╭╮╭╮
	╰┫┣╯╱╱╱╱╭╯╰╮╱╱┃┃┃┃
	╱┃┃╭━╮╭━┻╮╭╋━━┫┃┃┃╭┳━╮╭━━╮
	╱┃┃┃╭╮┫━━┫┃┃╭╮┃┃┃┃┣┫╭╮┫╭╮┃
	╭┫┣┫┃┃┣━━┃╰┫╭╮┃╰┫╰┫┃┃┃┃╰╯┃
	╰━━┻╯╰┻━━┻━┻╯╰┻━┻━┻┻╯╰┻━╮┃
	╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╭━╯┃
	╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╰━━╯
	""")


	additional_settings = ['-U']

	if not is_venv:
		additional_settings.append('--user')
		

	subprocess.check_call([sys.executable, '-m', 'pip', 'install', '-U', *additional_settings,
						   'telethon',
						   'cryptg',
						   'pillow',
						   'tgcrypto',
						   'pandas',
						   'loguru',
						   'filelock',
						   'psutil',
						   'requests',
						   'splinter',
						   'selenium',
						   'selenium-wire',
						   ])

	_create_message_files = False
	for p in DIR.values():
		if not os.path.exists(p):
			print("Creating missing directory: "+p)
			os.mkdir(p)
	if not os.path.isfile(DIR['PROXIES']+'/active.txt'):
		with open(DIR['PROXIES']+'/active.txt', 'w', encoding='utf-8'): pass
	if not os.path.isfile(DIR['PROXIES']+'/inactive.txt'):
		with open(DIR['PROXIES']+'/inactive.txt', 'w', encoding='utf-8'): pass

	##### Finished setting up


################### DEFAULTS ############################################

__basename = str(Path(os.path.dirname(os.path.realpath(__file__))).parent.absolute())
__basefile = os.path.splitext(os.path.basename(sys.argv[0]))[0]	

DIR = {
	'BASE' : __basename, 	# WORKING_DIR
	'DATA' : os.path.join(__basename,'data'),	# DATA_DIR
	'TELETHON_SESSION' : os.path.join(__basename,'data','telethon_session'), 	# SESSION_DIR
	'TELETHON_SESSION_TEMP' : os.path.join(__basename,'data','telethon_session','temp'), 	# 
	'TELETHON_SESSION_ARCHIVE' : os.path.join(__basename,'data','telethon_session','archive'), 	# 
	'PROXIES' : os.path.join(__basename,'data','proxies'), 	# GROUPS
	'TMP' : os.path.join(__basename,'tmp'),	# DATA_DIR
	'LOGS' : os.path.join(__basename,'logs'),	# LOG_DIR
}

### CONFIG DEFAULTS ###

if not os.path.isfile(DIR['BASE']+'/config.ini'):
	# Prepare some default configs
	import configparser
	CONFIG = configparser.ConfigParser(allow_no_value=True, delimiters=('='))
	CONFIG['Common'] = {
		'debug_mode' : 'off',
		'enable2fa' : 'false',
		'password2fa' : '[RANDOM]',
		'use_proxy' : 'true ',
		'change_proxy_after_failed_attempts' : '3',
		'max_threads' : '15',
	}
	CONFIG['my.telegram.org'] = {
		'10305366:c40842c619a91cb3f7327fd4c6465759':None,
		'17349:344583e45741c457fe1862106095a5eb':None,
		'21724:3e0cb5efcd52300aec5994fdfc5bdc16':None,
		'2899:36722c72256a24c1225de00eb6a1ca74':None,
		'16623:8c9dbfe58437d1739540f5d53c72ae4b':None,
		'10840:33c45224029d59cb3ad0c16134215aeb':None,
	}
	CONFIG['Devices'] = {
		'PC:Windows:7': None,
		'PC:Windows:8.1': None,
		'PC:Windows:8': None,
		'PC:Windows:10': None,
	}
	CONFIG['System Lang'] = {
		'aa|ab|ace|ach|ada|bas|bat|bax|bbj|ady|ae|af|af-NA|af-ZA|af|afa|afh|agq|agq-CM|agq|ain|ak|ak-GH|ak|akk|ale|alg|alt|am|am-ET|am|an|ang|anp|apa|ar|ar-001|ar-AE|ar-BH|ar-DJ|ar-DZ|ar-EG|ar-EH|ar-ER|ar-IL|ar-IQ|ar-JO|ar-KM|ar-KW|ar-LB|ar-LY|ar-MA|ar-MR|ar-OM|ar-PS|ar-QA|ar-SA|ar-SD|ar-SO|ar-SS|ar-SY|ar-TD|ar-TN|ar-YE|ar|arc|arn|arp|ars|art|arw|as|as-IN|as|asa|asa-TZ|asa|ast|ast-ES|ast|ath|aus|av|awa|ay|az|az-Cyrl|az-Cyrl-AZ|az-Latn|az-Latn-AZ|az|ba|bad|bai|bal|ban|bas|bas-CM|bas|bat|bax|bbj|be|be-BY|be|bej|bem|bem-ZM|bem|ber|bez|bez-TZ|bez|bfd|bg|bg-BG|bg|bh|bho|bi|bik|bin|bkm|bla|bm|bm-ML|bm|bn|bn-BD|bn-IN|bn|bnt|bo|bo-CN|bo-IN|bo|br|br-FR|br|bra|brx|brx-IN|brx|bs|bs-Cyrl|bs-Cyrl-BA|bs-Latn|bs-Latn-BA|bs|bss|btk|bua|bug|bum|byn|byv|ca|ca-AD|ca-ES|ca-FR|ca-IT|ca|cad|cai|car|cau|cay|cch|ce|ce-RU|ce|ceb|cel|cgg|cgg-UG|cgg|ch|chb|chg|chk|chm|chn|cho|chp|chr|chr-US|chr|chy|ckb|ckb-IQ|ckb-IR|ckb|cmc|co|cop|cpe|cpf|cpp|cr|crh|crp|cs|cs-CZ|cs|csb|cu|cus|cv|cy|cy-GB|cy|da|da-DK|da-GL|da|dak|dar|dav|dav-KE|dav|day|de|de-AT|de-BE|de-CH|de-DE|de-IT|de-LI|de-LU|de|del|den|dgr|din|dje|dje-NE|dje|doi|dra|dsb|dsb-DE|dsb|dua|dua-CM|dua|dum|dv|dyo|dyo-SN|dyo|dyu|dz|dz-BT|dz|dzg|ebu|ebu-KE|ebu|ee|ee-GH|ee-TG|ee|efi|egy|eka|el|el-CY|el-GR|el|elx|en|en-001|en-150|en-AG|en-AI|en-AS|en-AT|en-AU|en-BB|en-BE|en-BI|en-BM|en-BS|en-BW|en-BZ|en-CA|en-CC|en-CH|en-CK|en-CM|en-CX|en-CY|en-DE|en-DG|en-DK|en-DM|en-ER|en-FI|en-FJ|en-FK|enm|eo|eo|es|es-419|es-AR|es-BO|es-BR|es-BZ|es-CL|es-CO|es-CR|es-CU|es-DO|es-EA|es-EC|es-ES|es-GQ|es-GT|es-HN|es-IC|es-MX|es-NI|es-PA|es-PE|es-PH|es-PR|es-PY|es-SV|es-US|es-UY|es-VE|es|et|et-EE|et|eu|eu-ES|eu|ewo|ewo-CM|ewo|fa|fa-AF|fa-IR|fa|fan|fat|ff|ff-CM|ff-GN|ff-MR|ff-SN|ff|fi|fi-FI|fi|fil|fil-PH|fil|fiu|fj|fo|fo-DK|fo-FO|fo|fon|fr|fr-BE|fr-BF|fr-BI|fr-BJ|fr-BL|fr-CA|fr-CD|fr-CF|fr-CG|fr-CH|fr-CI|fr-CM|fr-DJ|fr-DZ|fr-FR|fr-GA|fr-GF|fr-GN|fr-GP|fr-GQ|fr-HT|fr-KM|fr-LU|fr-MA|fr-MC|fr-MF|fr-MG|fr-ML|fr-MQ|fr|frm|fro|frr|frs|fur|fur-IT|fur|fy|fy-NL|fy|ga|ga-IE|ga|gaa|gay|gba|gd|gd-GB|gd|gem|gez|gil|gl|gl-ES|gl|gmh|gn|goh|gon|gor|got|grb|grc|gsw|gsw-CH|gsw-FR|gsw-LI|gsw|gu|gu-IN|gu|guz|guz-KE|guz|gv':None,
	}

	with open(DIR['BASE']+'/config.ini', 'w', encoding='utf-8') as configfile:
		CONFIG.write(configfile)

######################################################################

# TODO look at https://github.com/pwaller/pyfiglet

_startup_time = time.time()

banner = ("""

████████╗███████╗██╗░░░░░███████╗░██████╗░██████╗░░█████╗░███╗░░░███╗  ████████╗░█████╗░░█████╗░██╗░░░░░░██████╗
╚══██╔══╝██╔════╝██║░░░░░██╔════╝██╔════╝░██╔══██╗██╔══██╗████╗░████║  ╚══██╔══╝██╔══██╗██╔══██╗██║░░░░░██╔════╝
░░░██║░░░█████╗░░██║░░░░░█████╗░░██║░░██╗░██████╔╝███████║██╔████╔██║  ░░░██║░░░██║░░██║██║░░██║██║░░░░░╚█████╗░
░░░██║░░░██╔══╝░░██║░░░░░██╔══╝░░██║░░╚██╗██╔══██╗██╔══██║██║╚██╔╝██║  ░░░██║░░░██║░░██║██║░░██║██║░░░░░░╚═══██╗
░░░██║░░░███████╗███████╗███████╗╚██████╔╝██║░░██║██║░░██║██║░╚═╝░██║  ░░░██║░░░╚█████╔╝╚█████╔╝███████╗██████╔╝
░░░╚═╝░░░╚══════╝╚══════╝╚══════╝░╚═════╝░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░░░░╚═╝  ░░░╚═╝░░░░╚════╝░░╚════╝░╚══════╝╚═════╝░

""")
VERSION = '2.1'
banner = banner  + "   Version: "+ VERSION

# print(banner)
print("\n\n========================================")
print(" ---- TelegramTools")
print("========================================")
# print(" (1) Terminal (Mac/Linux) or (2) Command Prompt (Windows)\n")

# clearType = input(' [?] What is your System? (1 or 2): ')

if any([k in sys.argv for k in ['--help','-h']]):
	print("""
Usage:   
  python3 TelegramTools.py [options]
  	or
  py TelegramTools.py [options]

Main Options:
  -s, --session <name>         Automatically uses and login to stated User Session.
  -j, --job <name>             Selects and works with stated Job.

General Options:
  -h, --help                    Show help.
  --debug                       Records all levels of error and all debug logs.
  --simulation                  Runs in simulation mode. Database updates, SMS and invites are not actioned in Simulation mode.
  --no-wait                     Waiting time between actions are ommitted.
  --install, --update           Install/Update TelegramTools application and all dependencies.
  --uninstall                   Removes the CRON job that was set up during installation. (Windows users must manually remove from Task Scheduler)
  -v, --verbose                 Give more output. Displays all messages on screen.
  -V, --version                 Show version and exit.
  --no-proxy                    Disables use of proxy, overriding configuration setting.
  --skip-install                Run app as is, without installing libraries or preparing folders. May experience errors.

	""")
	exit()

if any([k in sys.argv for k in ['--version','-V']]):
	print("Version: "+VERSION)
	exit()

if ('--install' in sys.argv or '--update' in sys.argv):
	_telegram_tools_install()

print(" \nLoading. Please wait ......")


################### LOAD DEPENDENCIES #################

try:	
	sys.path.append(os.path.abspath(os.path.join(DIR['BASE'],'lib')))
	from helpers import *
except Exception as e:
	print("")
	# print(str(e))
	_telegram_tools_install()
	exit("\nMissing dependencies have been installed. Please execute script again.\n")


############ VERIFY FOLDER STRUCTURE ##############
for p in DIR.values():
	if not os.path.exists(p):
		if '--skip-install' not in sys.argv: _telegram_tools_install()
		# _run_setup = True if '--skip-install' not in sys.argv else False
		break

############
if not any([k in sys.argv for k in ['--verbose','-v']]):
	try:
		log.remove(0)
	except:
		pass
	log.add(sys.stderr, level="WARNING", enqueue=True, backtrace=True, diagnose=True)	

SESSION_NAME = JOB_NAME = JOB_TYPE = ""

if any([o in sys.argv for o in ['-s','--session']]):
	try:
		_i = sys.argv.index('--session')
	except:
		_i = sys.argv.index('-s')
	if os.path.isfile(os.path.join(DIR['TELETHON_SESSION'],sys.argv[_i+1]+".session")):
		SESSION_NAME = sys.argv[_i+1]
if any([o in sys.argv for o in ['-j','--job']]):
	try:
		_i = sys.argv.index('--job')
	except:
		_i = sys.argv.index('-j')
	if "/" in sys.argv[_i+1]:
		_job = sys.argv[_i+1].split('/')
		if os.path.isdir(os.path.join(DIR['JOBS'],_job[0],_job[1])):
			JOB_NAME = _job[1]
			JOB_TYPE = _job[0]

if JOB_NAME:
	log.add(DIR['LOGS']+f"/{__basefile}{'_'+JOB_NAME if JOB_NAME else ''}.log", format="{time:YYYY-MM-DD at HH:mm:ss} "+JOB_NAME+" "+SESSION_NAME+" {level} | {name}:{function}:{line} {message}", level="DEBUG" if DEBUG else "INFO", retention="15 days", enqueue=True, backtrace=True, diagnose=True)	
	log.add(DIR['LOGS']+f"/{__basefile}.log", format="{time:YYYY-MM-DD at HH:mm:ss} "+SESSION_NAME+" {level} | {name}:{function}:{line} {message}", level="WARNING", retention="15 days", enqueue=True, backtrace=True, diagnose=True)	
else:
	log.add(DIR['LOGS']+f"/{__basefile}.log", format="{time:YYYY-MM-DD at HH:mm:ss} "+SESSION_NAME+" {level} | {name}:{function}:{line} {message}", level="DEBUG" if DEBUG else "INFO", retention="15 days", enqueue=True, backtrace=True, diagnose=True)	

log.debug("Python Virtualenv Active: "+str(is_venv))

if SIMULATION:
	log.debug("Running in SIMULATION mode. No real messages will be sent.")
if NOWAIT:
	log.debug("NO WAIT mode is on. Waiting times omitted.")
if DEBUG:
	log.debug("DEBUG mode is on.")

############ CORE CLASS ############
class TelegramTools():

	"""
		Base class contianing helpers and data manipulation functions.
	"""

	""" Global variables and defaults """
	_session_metadata_defaults = {
	    "session_file": "",
	    "phone": "",
	    "register_time": 0,
	    "app_id": "",
	    "app_hash": "",
	    "sdk": "",
	    "app_version": "",
	    "device": "",
	    "last_check_time": 0,
	    "avatar": "",
	    "first_name": "",
	    "last_name": "",
	    "username": None,
	    "sex": "",
	    "lang_pack": "",
	    "system_lang_pack": "",
	    "proxy": [],
	    "ipv6": False,
	    "twoFA": "",
	    "telegram-tools":
	    {
			"active" : False,
			"daily_sms_count" : 0,
			"daily_invite_count" : 0,
			"total_sms_success" : 0,
			"total_sms_fail" : 0,
			"next_use" : 0,
			"role" : [],
			"joined_groups" : [],
			"failed_groups" : [],
			"locked" : False,
			"basename" : "",
			"geo" : "", # TODO https://stackoverflow.com/questions/24678308/how-to-find-location-with-ip-address-in-python
			"user_id" : 0,
			"FloodErrors" : 0,
			"contacts": []

	    },
	    "NurtureClients" : {
	    	"next_nurture_time" : 0,
	    	"last_sms_contacts" : 0,
	    	"last_picture_update" : 0,
	    	"last_join_chat" : 0

	    }
	}
	
	_job_config_defaults = {
		'Common' : {
			'active' : 'False'	,
			'daily_max' : '30'	,
			'target_type' : ''	,
			'audience_type' : 'users'	,
			'audience_filter' : 'all'	,
			'method' : ''	,
			'run_options' : ''	,
			'job_type' : ''	,
			'total_limit' : '100'	,
			'notification_username' : ''	,
			'minutes_between_operations' : '15'	,
			'days_of_operation' : '0,1,2,3,4,5,6'	,
			'start_hour' : '1'	,
			'end_hour' : '23'	,
			'leave_group_on_completion' : 'True'	,
			'threads' : '5'
		},
		'Worker Filter' :{
			'basename' : ''	,
			'sex' : -1,
			'avatar' : 'both',
			'phone_prefix' : '',

		},
		'Workers' : {

		},			
	}


	@log.catch()
	def __init__(self):
		
		self._load_time = _startup_time

		# self.Config = self._load_core_config()
		log.debug("Loaded Core Config")
		log.debug(str({section: dict(self.Config()[section]) for section in self.Config().sections()}))

		self.JobName = self.JobType = self._job_config = None
		self.JobPaths = self._active_jobs = {}

		if JOB_NAME:
			self.JobType = JOB_TYPE
			self.JobName = JOB_NAME
			self._job_config = self.LoadJobConfig(JOB_TYPE,JOB_NAME)
			self.JobPaths = self.LoadJobPaths(JOB_TYPE,JOB_NAME)
			log.debug(f"Loading Job '{JOB_TYPE}/{JOB_NAME}' with Config:")
			log.debug(str({section: dict(self._job_config[section]) for section in self._job_config.sections()}))
			log.debug("self.JobPaths = "+str(self.JobPaths))

		self.SessionName = None

		if SESSION_NAME:
			self.SessionName = SESSION_NAME


	@log.catch()
	def __del__(self):
		log.debug("Self desctructing core.py")
		_interval = str(time.time() - self._load_time )
		log.debug(f"Total time to run app: {_interval[:_interval.rfind('.')+3]} seconds")
		pass

	"""

		CONFIG

	"""

	@log.catch()
	def Require(self,r):
		log.debug("Checking requirements..")
		if r.lower() == 'job' and not JOB_NAME:
			log.warning("Valid Job Name is Required.")
			exit()
		if r.lower() == 'session' and not SESSION_NAME:
			log.warning("Valid Session name is Required.")
			exit()
		log.debug("Checking requirements - PASS")

	@log.catch()
	def _load_core_config(self):
		""" Load core configuration from config file and sets simulation/debug modes 
			stores config in self.Config
		"""
		self._config = configparser.ConfigParser(allow_no_value=True, delimiters=('='))
		if os.path.isfile(DIR['BASE']+'/config.ini'):
			self._config.read(DIR['BASE']+'/config.ini')

		if '--no-proxy' in sys.argv:
			if 'use_proxy' in self._config['Common']:
				self._config['Common']['use_proxy'] = False

		return self._config

	@log.catch()
	def Config(self):
		""" Returns the configparser object """
		try:
			self._config
		except:
			self._load_core_config()
		return self._config

	@log.catch()
	def SaveConfig(self):
		""" saves the configuration from self.Config """
		# avoid this in case of CLI overide options
		pass


	"""

		JOBS

	"""

	@log.catch()
	def JobConfig(self):
		""" Returns the configparser object """
		try:
			self._job_config
		except:
			self._job_config = self.LoadJobConfig()
		return self._job_config

	@log.catch()
	def LoadJobConfig(self, job_type='',job_name=''):
		""" loads job config from config file based on job name, in job folder.
			if job=='', fetch from sys.argv if present.
			stores config in self._job_config
		"""
		job_type = self.JobType if not job_type and self.JobType else job_type 
		job_name = self.JobName if not job_name and self.JobName else job_name

		if job_name and job_type:
			cfg = configparser.ConfigParser(allow_no_value=True, delimiters=('='))
			if os.path.isfile(self.Path(DIR['JOBS'],job_type,job_name,'config.ini')):
				cfg.read(self.Path(DIR['JOBS'],job_type,job_name,'config.ini'))
			else:
				# Prepare some default configs
				cfg.read_dict(self._job_config_defaults)
				# self.SaveJobConfig(job_type,job_name,cfg)

			return cfg		
	
	@log.catch()
	def LoadJobPaths(self,job_type='',job_name=''):
		job_type = self.JobType if not job_type and self.JobType else job_type 
		job_name = self.JobName if not job_name and self.JobName else job_name
		if job_name and job_type:
			_p = self.Path(DIR['JOBS'],job_type,job_name)
			_paths = {'base':_p}
			# prepare files/folders
			folders = ['data','config','report','messages','tmp','target']
			for f in folders:
				_paths[f] = self.Path(_p,f)
				if not os.path.exists(self.Path(_p,f)):
					os.mkdir(self.Path(_p,f))
			return _paths


	@log.catch()
	def SaveJobConfig(self,job_type=None, job_name=None, config=None):
		""" saves job config based on job name, in job folder."""
		if not config and self._job_config:
			config = self._job_config
		job_type = self.JobType if not job_type and self.JobType else job_type 
		job_name = self.JobName if not job_name and self.JobName else job_name

		if config:
			file = self.Path(DIR['JOBS'],job_type,job_name,'config.ini')
			lock = FileLock(file+'.lock',timeout=15)
			try:
				if os.path.exists(file):
					lock.acquire()
				with open(file, 'w', encoding='utf-8') as configfile:
					config.write(configfile)
			except Exception as e:
				log.error(e)
			finally:
				lock.release()	

	@log.catch()
	def IsJobActive(self,job_type='', job_name=''):
		if not job_type and self.JobType and self._job_config:
			job_type = self.JobType
			job_name = self.JobName
		try:
			self._active_jobs[job_type][job_name]
		except:
			if job_type and job_name:
				if not job_type in self._active_jobs: self._active_jobs[job_type] = {}
				if job_type == self.JobType and job_name == self.JobName:
					self._active_jobs[job_type][job_name] = self._job_config.getboolean('Common','active',fallback=False)
				elif os.path.isfile(self.Path(DIR['JOBS'],job_type,job_name,'config.ini')):
					cfg = configparser.ConfigParser(allow_no_value=True)
					cfg.read(self.Path(DIR['JOBS'],job_type,job_name,'config.ini'))
					self._active_jobs[job_type][job_name] = cfg.getboolean('Common','active',fallback=False)
				else:
					log.warning("Job name and type could not be determined.")
					return False
			else:
				log.warning("Job name and type could not be determined.")
				return False
		return self._active_jobs[job_type][job_name]

	@log.catch()
	def IsJobAllowed(self,job_type='',job_name=''):
		# checks to see if the job is allowed to proceed.
		if '--force-run' in sys.argv: return True
		
		t = time.strftime("%w %H %M", time.gmtime()).split(" ")
		log.debug("Time Format: 'DayOfWeek', 'Hour', 'Minute' = "+str(t))
		if not job_type and self.JobType and self._job_config:
			job_type = self.JobType
			job_name = self.JobName
			cfg = self._job_config
		elif job_name and job_type:
			cfg = self.LoadJobConfig(job_type,job_name)
		else:
			log.warning("Could not determine Job type/name.")
			return False

		# not sending at midnight. Reset counter
		if int(t[1]) == 0 and int(t[2]) >= 50: 	
			log.debug("Resetting counters for all session workers.")
			sessions_list = list(map(lambda s: s[:s.find('.')], filter(lambda f: f.endswith('.json'), os.listdir(DIR['TELETHON_SESSION']))))
			for s in sessions_list:
				try:
					self.UpdateSessionMetadata(s,{'telegram-tools':{'daily_sms_count':0,'daily_sms_count':0}})
				except Exception as e:
					log.error(e)

		# check is active
		if not self.IsJobActive(job_type,job_name):
			log.debug(f"Job '{job_type}/{job_name}' is not active.")
			return False

		if str(t[0]) not in cfg.get('Common','days_of_operation',fallback='0,1,2,3,4,5,6'):
			log.debug(f"Job '{job_type}/{job_name}' not sending today.")
			return False

		if int(t[1]) < cfg.getint('Common','start_hour',fallback=0) or int(t[1]) >= cfg.getint('Common','end_hour',fallback=24):
			log.debug(f"Job '{job_type}/{job_name}' Time is outside of start and end time.")
			return False

		nr = cfg.getint('Common','next_run',fallback=0)
		if time.time() < nr and nr > 0:
			log.debug("Next execute time has not been reached.")
			return False
		
		log.debug("Job is allowed to proceed.")
		return True

	@log.catch()
	def JobClose(self):
		log.debug(f"Closing job {self.JobType}/{self.JobName}.")
		if self.JobName and self.JobType and self.JobConfig():			
			self.JobConfig()['Common']['next_run'] = str(int(time.time()+self.JobConfig().getint('Common','minutes_between_operations',fallback=5)*MINUTE_IN_SECONDS))
			self.SaveJobConfig()
		else:
			log.error("JobName or JobType or JobConfig is missing.")


	@log.catch()
	def GetJobDatabase(self,job_type='',job_name=''):
		import pandas as pd
		job_type = self.JobType if not job_type and self.JobType else job_type 
		job_name = self.JobName if not job_name and self.JobName else job_name
		if job_type and job_name:
			data = ['id','username','title','count_total','count_complete','count_fail','status']
			try:
				db = pd.read_csv(self.Path(DIR['JOBS'],job_type,job_name,'targets.csv'), index_col=0, header=0, na_filter=False)
			except:
				db = pd.DataFrame(columns=data)
			return db
		return None

	@log.catch()
	def UpdateJobDatabase(self,job_type='',job_name='',data=None):
		import pandas as pd
		""" updates database for job based on job type 
				job types: sms, invite, register, autopost, answer_machine, audience_collect, subscription_cheat, views_cheat, bot_referral_cheat, reaction_cheat, vote_cheat

			Universal structure of database:
				location: data/jobs/{job_type}/{job_name}/targets/targets.csv
				Structure:
				id	username	title 	count_total	count_complete count_fail 	status
				?????

		"""
		try:
			data.shape[0]
		except:
			return False

		job_type = self.JobType if not job_type and self.JobType else job_type 
		job_name = self.JobName if not job_name and self.JobName else job_name
		try:
			if isinstance(data,object):
				data.to_csv(self.Path(DIR['JOBS'],job_type,job_name,'targets.csv'))
				return True
		except Exception as e:
			log.error(e)

		return False		

	@log.catch() # TODO revise
	def ListProjects(self):
		# show existing projects
		try:
			pr = []
			pa = DIR['SUBPROCESSES']
			if len(os.listdir(pa)) > 0:
				for p in os.listdir(pa):
					if os.path.isdir(os.path.join(pa, p)):
						pr.append(p)
		except Exception as e:
			log.error(e)			

		return pr


	"""

		SESSIONS

	"""

	@log.catch()
	def ListSessions(self,all=False):
		_p = DIR['TELETHON_SESSION']
		sessions_list = list(
			map(lambda s: s[0:s.rfind(".")], filter(lambda f: f.endswith('.json' if not all else '.session'), os.listdir(_p))))
		return sessions_list

	@log.catch()
	def UpdateSessionMetadata(self,session='',values:dict = {} ):
		if SIMULATION:
			log.info("Skipping Updating of Database in Simulation mode.")
			return
		log.debug("Updating Session Metadata")
		log.debug(values)
		if not session and SESSION_NAME:
			session = SESSION_NAME
		if session:
			if os.path.exists(self.Path(DIR['TELETHON_SESSION'],session+".json")):
				try:
					with open(self.Path(DIR['TELETHON_SESSION'],session+".json"),'r+',encoding='utf-8') as f:
						data = json.loads(f.read())
						if values:
							for k in values.keys():
								if isinstance(values[k],dict):
									if not k in data:
										data[k] = {}
									for kk in values[k]:
										data[k][kk] = values[k][kk]
								else:
									data[k] = values[k]
						# save
						f.seek(0)
						f.truncate()
						w = json.dumps(data)
						f.write(w)
						return True
				except Exception as e:
					log.error(e)	
			else:
				# Metadata file is missing.
				try:
					with open(self.Path(DIR['TELETHON_SESSION'],session+".json"),'w',encoding='utf-8') as f:
						data = self._session_metadata_defaults
						if values:
							for k in values.keys():
								if isinstance(values[k],dict):
									if not k in data:
										data[k] = {}
									for kk in values[k]:
										data[k][kk] = values[k][kk]
								else:
									data[k] = values[k]
						# save
						w = json.dumps(data)
						f.write(w)
						return True
				except Exception as e:
					log.error(e)

		return False


	@log.catch()
	def GetSessionMetadata(self,session=''): # TODO change to GetSessionMetadata()
		if not session and SESSION_NAME:
			session = SESSION_NAME
		if session:
			if os.path.exists(self.Path(DIR['TELETHON_SESSION'],session+".json")):
				try:
					with open(self.Path(DIR['TELETHON_SESSION'],session+".json"),'r',encoding='utf-8') as f:
						data = json.loads(f.read())
						if self.Config().getboolean('Common','use_proxy',fallback=False) and not data['proxy']:
							data['proxy'] = self.GetProxy() 
							self.UpdateSessionMetadata(session,data)
						for k in self._session_metadata_defaults.keys():
							if isinstance(self._session_metadata_defaults[k],dict):
								if not k in data:
									data[k] = {}
								for kk in self._session_metadata_defaults[k]:
									if not kk in data[k]:
										data[k][kk] = self._session_metadata_defaults[k][kk]
							elif not k in data:
								data[k] = self._session_metadata_defaults[k]	
						return data
				except Exception as e:
					log.error(e)	
			else:
				# Metadata file is missing. Possibly banned
				data = self._session_metadata_defaults
				if self.Config().getboolean('Common','use_proxy',fallback=False) and not data['proxy']:
					data['proxy'] = self.GetProxy() 
					self.UpdateSessionMetadata(session,data)
				return data
		
		return False


	@log.catch()
	def IsSessionAllowed(self,session=''):
		# checks to see if the worker is allowed to work

		data = self.GetSessionMetadata(session)

		if not data:
			return False

		# check personal worker allowance
		try:
			if not bool(data['telegram-tools']['status_active']):
				log.debug(f"Session '{session}' is not active.")
				return False
			if int(data['telegram-tools']['next_use']) < time.time_ns():
				log.debug(f"Session '{session}' is still cooling down.")
				return False
			# if 'worker' not in data['telegram-tools']['role']:
			# 	log.debug(f"Session '{session}' does not have the 'worker' role.")
			# 	return False

		except Exception as e:
			log.error(e)
			return False


		# get job config
		if self._job_config:
			try:
				# do checks for specific job
				jtype = self._job_config.get('Common','job_type',fallback="")
				if jtype == 'sms':
					if int(data['telegram-tools']['daily_sms_count']) >= self._job_config.getint('Common','daily_max',fallback=20):
						log.debug("Session has reached daily SMS count.")
						return False
				if jtype == 'invite':
					if int(data['telegram-tools']['daily_invite_count']) >= self._job_config.getint('Common','daily_max',fallback=20):
						log.debug("Session has reached daily Invite count.")
						return False



			except Exception as e:
				log.error(e)
				return False

		return True

	@log.catch()
	def ListSessionIds(self):
		sessions = self.ListSessions()
		ids = []
		for s in sessions:
			m = self.GetSessionMetadata(s)
			if m:
				if m['telegram-tools']['user_id']: ids.append(m['telegram-tools']['user_id'])
		return ids
	
	@log.catch()
	def SessionsDataAsDict(self,**filters):
		""" TODO filters kwargs to select sessions based on filters criteria.
					eg. arg as filters passed:
								active = True, sex = 1
					check for key=variable and value=value
					try to match types.
		"""

		sessions = self.ListSessions()
		out = {}
		for s in sessions:
			m = self.GetSessionMetadata(s)
			if m:
				out[s] = m
		return out

	@log.catch()
	def IncrementSessionFloodError(self,session):
		metadata = self.GetSessionMetadata(session)
		self.UpdateSessionMetadata(session, {'telegram-tools':{'FloodErrors':int(metadata['telegram-tools']['FloodErrors']) + 1 }})

	@log.catch()
	def SelectClient(self,all=False,handle_selection=True):
		print(gr+"\n========================================")
		print(gr+" ---- Loading available Client Accounts")
		print(gr+"========================================")
		sd = self.SessionsDataAsDict()
		h = '  # '
		h = h + sp(10-len(h)) + 'Session'
		h = h + sp(30-len(h)) + 'Name'
		h = h + sp(50-len(h)) + 'Username'
		h = h + sp(75-len(h)) + 'Phone'
		h = h + sp(95-len(h)) + 'Proxy'
		h = h + sp(113-len(h)) + 'Age'
		h = h + sp(123-len(h)) + 'Last Login'
		h = h + sp(135-len(h)) + 'Sex'
		h = h + sp(140-len(h)) + '2FA'
		h = h + sp(145-len(h)) + 'Active'
		h = h + sp(155-len(h)) + 'Role'
		print(re+h)
		h = '---'
		h = h + sp(10-len(h)) + '---'
		h = h + sp(30-len(h)) + '---'
		h = h + sp(50-len(h)) + '---'
		h = h + sp(75-len(h)) + '---'
		h = h + sp(95-len(h)) + '---'
		h = h + sp(113-len(h)) + '---'
		h = h + sp(123-len(h)) + '---'
		h = h + sp(135-len(h)) + '---'
		h = h + sp(140-len(h)) + '---'
		h = h + sp(145-len(h)) + '---'
		h = h + sp(155-len(h)) + '---'
		print(re+h)
		sessions = []
		i = 0
		for s in sd.keys():
			sessions.append(s)
			p = f' [{i}] '
			p = p + sp(10-len(p)) +f"({s})"
			p = p + sp(30-len(p)) +f"{sd[s]['first_name']} {sd[s]['last_name'] if sd[s]['last_name'] else ''}"
			p = p + sp(50-len(p)) +f"{sd[s]['username'] if sd[s]['username'] else ''}"
			p = p + sp(75-len(p)) +f"+{sd[s]['phone']}"
			p = p + sp(95-len(p)) +f"{sd[s]['proxy'][1] if sd[s]['proxy'] else ''}"
			age = str((int(time.time()) - int(sd[s]['register_time']))/DAY_IN_SECONDS)
			p = p + sp(113-len(p)) +age[:age.rfind('.')] + ' days'
			lconn = str((int(time.time()) - int(sd[s]['last_check_time']))/HOUR_IN_SECONDS)
			p = p + sp(123-len(p)) +lconn[:lconn.rfind('.')] + ' hours'
			sex = 'M' if str(sd[s]['sex']) == '1' else ''
			sex = 'F' if str(sd[s]['sex']) == '0' else sex
			p = p + sp(135-len(p)) +f"{sex}"
			p = p + sp(140-len(p)) +f"{'Y' if sd[s]['twoFA'] else 'N'}"
			p = p + sp(145-len(p)) +f"{bool(sd[s]['telegram-tools']['active'])}"
			p = p + sp(155-len(p)) +f"{','.join(sd[s]['telegram-tools']['role'])}"
			print((gr if bool(sd[s]['telegram-tools']['active']) else ye) + p)
			i+=1
			
		chosen = None
		if handle_selection:
			while True:	
				choice = input(ye+"\n [?] Login to which account? ")
				if not choice.isnumeric(): continue
				if int(choice) >= len(sessions) or int(choice) < 0: continue
				chosen = sessions[int(choice)]
				break

			if chosen:
				return chosen
		else:
			return sessions

		return False



	""" 

		HELPERS

	"""

	@log.catch()
	def Path(self,*args):
		if len(args)>0:
			return os.path.join(*args)

	@log.catch()
	def FormatTime(self):
		return time.strftime("%x %X",time.localtime())

	@log.catch()
	def GetProxy(self,pref_ip=''):
		
		proxy_list = []
		proxy = None

		with open(DIR['PROXIES']+'/inactive.txt', 'r', encoding='utf-8') as f:
			ignore = f.read()
		with open(DIR['PROXIES']+'/active.txt', 'r', encoding='utf-8') as f:
			for l in f.readlines():
				if "TELETHON END" in l:
					break
				if l == '' or ',' not in l or '#' in l or '//' in l:
					continue
				proxy_list.append(l.replace("\n","").split(','))

		if len(proxy_list)==0:
			log.warning("No proxy available to use.")
			return False

		if pref_ip:
			for p in proxy_list:
				if pref_ip in p[0]:
					proxy = p
		if not proxy:
			proxy = random.choice(proxy_list)

		while proxy[0] in ignore :
			# print("choosing again... "+proxy[0])
			proxy = random.choice(proxy_list)

		log.debug("Using proxy: " + str(proxy))

		# self.Config['proxy'].clear()

		# new format: list. Compatible with json metadata. Must convert to dict for Telethon
		# set_proxy(proxy_type, addr[, port[, rdns[, username[, password]]]])
		# https://github.com/Anorov/PySocks#usage-1
		return [ProxyType.SOCKS5.value, proxy[0], int(proxy[1]), True, proxy[2], proxy[3] ]


		"""
		  proxy = {
			'proxy_type': 'socks5', # (mandatory) protocol to use (see above)
			'addr': '1.1.1.1', # (mandatory) proxy IP address
			'port': 5555, # (mandatory) proxy port number
			'username': 'foo', # (optional) username if the proxy requires auth
			'password': 'bar', # (optional) password if the proxy requires auth
			'rdns': True # (optional) whether to use remote or local resolve,default remote
			}
		"""

	@log.catch()
	def PrepareProxyForTelethon(self,proxy):
		""" Prepares the proxy input for Telegram Client - convert to dict """
		if isinstance(proxy,list):
			if proxy[4] != '':
				# use username and password
				return {
					'proxy_type': proxy[0],
					'addr': proxy[1], 
					'port': int(proxy[2]),	
					'username': proxy[4], # (optional) username if the proxy requires auth
					'password': proxy[5], # (optional) password if the proxy requires auth
				}	
			else:
				return {
					'proxy_type': proxy[0], #ProxyType.SOCKS5,
					'addr': proxy[1], 
					'port': int(proxy[2])	
				}	
		return proxy		

	@log.catch()
	def GetRandomApi(self):
		if 'my.telegram.org' in self.Config().sections():
			api_id = [x for x in self.Config()['my.telegram.org'] if x[0:1].isnumeric()]
			if len(api_id)>0:
				return random.choice(api_id).split(":")
		return None

	@log.catch()
	def GetRandomDevice(self):
		if 'Devices' in self.Config().sections():
			d_list = [x for x in self.Config()['Devices']]
			if len(d_list)>0:
				return random.choice(d_list).split(":")
		return None		

	@log.catch()
	def GetRandomLangCode(self):
		if 'System Lang' in self.Config().sections():
			d_list = [x for x in self.Config()['System Lang']]
			if len(d_list)>0:
				return random.choice("|".join(d_list).split("|"))
		return None		

	@log.catch()
	def GeneratePassword(self):
		import string
		characters = list(string.ascii_letters + string.digits)
		## shuffling the characters
		random.shuffle(characters)

		## picking random characters from the list
		password = []
		for i in range(9):
			password.append(random.choice(characters))

		## shuffling the resultant password
		random.shuffle(password)

		## converting the list to string
		## printing the list
		return "".join(password)

	"""
		
		SUBPROCESS

	"""

	@log.catch()
	def Popen(self,args,  bufsize=- 1, executable=None, stdin=None, stdout=None, stderr=None,
			preexec_fn=None, close_fds=True, shell=False, cwd=None, env=None, universal_newlines=None, 
			startupinfo=None, creationflags=0, restore_signals=True, start_new_session=False, pass_fds=(), 
			*, encoding=None, errors=None, text=None,
			restart=False,  # custom: option to kill and restart running process
			name='',		# custom: subprocess name identifier used to check if process already running
			sys_executable = False, # custom: if true, prepends sys.executable to args
		):
		"""
		equivalent to subprocess.Popen()

		"""
		import psutil

		# if not cwd: cwd = DIR['BASE']
		if not cwd: cwd = os.getcwd()

		_section = replace_all(str([*args]), {'[':'',']':''})
		log.debug("Subprocess section: "+str(_section))

		# running process ids path
		if name:
			pcfg = configparser.ConfigParser(allow_no_value=True,delimiters=('='))
			if os.path.isfile(self.Path(DIR['SUBPROCESSES'],name+".ini")):	
				pcfg.read(self.Path(DIR['SUBPROCESSES'],name+".ini"))
				if pcfg.getint(_section,'process_id',fallback=0)>0:
					old_pid = pcfg.getint(_section,'process_id')
					p = None
					try:
						log.debug(f"Subprocess {name}[{_section}] (pid:{int(old_pid)}) found. Checking if still running.")
						p = psutil.Process(int(old_pid))
						print(p)
						if restart:
							p.terminate()
						else: 
							# currently running and dont want to restart.
							log.debug(f"Subprocess {name}[{_section}] (pid:{int(old_pid)}) already running and restart=False. Skipping.")
							return p

					except psutil.NoSuchProcess:
						pass
					except Exception as e:
						log.error(e)

					try:
						if p:
							gone, alive = psutil.wait_procs(p, timeout=3)
							for p2 in alive:
								log.warning(f"Killing a live process with restart={str(restart)}...")
								p2.kill()
					except Exception as e:
						log.error(e)
		
			if not pcfg.has_section(_section):
				pcfg.add_section(_section)

		# ready to run new subprocess
		try:
			if sys_executable:
				args = [sys.executable, *args]

			log.debug(f"Subprocess args: {args}")
			proc = subprocess.Popen(args,
                bufsize=bufsize,
                executable=executable,
                stdin=stdin,
                stdout=None if not stdout else stdout,
                stderr=None if not stderr else stderr,
				preexec_fn=preexec_fn,
				close_fds=close_fds,
				shell=shell,
				cwd=cwd,
				env=env,
				universal_newlines=universal_newlines,
				startupinfo=startupinfo,
				creationflags=creationflags,
				restore_signals=restore_signals,
				start_new_session=start_new_session,
				pass_fds=pass_fds,
				encoding=encoding,
				errors=errors,
				text=text,
			)

			if name:
				pcfg[_section]['process_id'] = str(proc.pid)
				log.info(f"Subprocess {name}[{_section}] started with process ID: {proc.pid}")
				# save config
				with open(self.Path(DIR['SUBPROCESSES'],name+".ini"), 'w', encoding='utf-8') as configfile:
					pcfg.write(configfile)
			else:
				log.info(f"Subprocess [{_section}] started with process ID: {proc.pid}.")
				log.warning("Subprocess ID was not saved. To avoid multiple instances of the exact same process, use the 'name' argument.")

			return proc

		except Exception as e:
			log.error(e)	


		return None


	@log.catch()
	def OpenSubprocess(self,file, sys_executable=True, *ops, **kwargs ):
		if os.path.isfile(DIR['BASE']+'/'+str(file)):
			if sys_executable:
				args1 = [sys.executable, DIR['BASE']+'/'+file]
			else:
				args1 = [DIR['BASE']+'/'+file]
			if ops:
				_ops = []
				for o in ops:
					if isinstance(o,list):
						for _o in o:
							_ops.append(_o.strip())
					else:
						_ops.append(o.strip())
			
			_restart = False

			if kwargs:
				for a in kwargs.keys():
					if a == 'restart':
						_restart = bool(kwargs[a])

			subprocess_config = configparser.ConfigParser(allow_no_value=True,delimiters=('='))
			subprocess_config.read(DIR['SUBPROCESSES']+'/'+str(os.path.splitext(file)[0])+'.txt')

			_section = replace_all(str([file,*_ops]), {'[':'',']':''})

			log.debug("Sending to Subprocess: "+str([file,*_ops]))

			try:
				# check if subprocess already setup:
				if subprocess_config.has_section(_section):
					old_pid = subprocess_config.get(_section,'process_id',fallback=0)
					# process exists. Need to make sure it is closed.
					p = None
					if int(old_pid)>0:
						try:
							p = psutil.Process(int(old_pid))
							if _restart:
								p.terminate()
							else: 
								# currently running and dont want to restart.
								log.info(f"Subprocess {os.path.splitext(file)[0]}[{_section}] (pid:{int(old_pid)}) already running and restart=False. Skipping.")

						except psutil.NoSuchProcess:
							pass
						except Exception as e:
							log.error(e)
						try:
							if p:
								gone, alive = psutil.wait_procs(p, timeout=3)
								for p2 in alive:
									log.warning(f"Killing a live process with restart={str(restart)}...")
									p2.kill()
						except Exception as e:
							log.error(e)
					else:
						# clear section of old pid that are not active
						# subprocess_config[_section].clear() # not required because we use the same key
						pass
					
				# ready to run new subprocess
				try:
					proc = subprocess.Popen([*args1,*_ops],
			                stdout=subprocess.PIPE, 
			                stderr=subprocess.STDOUT)

					if not subprocess_config.has_section(_section):
						subprocess_config.add_section(_section)
					subprocess_config[_section]['process_id'] = str(proc.pid)
					log.info(f"Subprocess {os.path.splitext(file)[0]}[{_section}] started with process ID: {proc.pid}")

				except Exception as e:
					log.error(e)	

			except Exception as e:
				log.error(e)				
			
			try:
				# save config
				with open(DIR['SUBPROCESSES']+'/'+str(os.path.splitext(file)[0])+'.txt', 'w', encoding='utf-8') as configfile:
					subprocess_config.write(configfile)

				"""
					Subprocess Job config file
					eg.
						[Common]
						target_users = admin # OR member OR all (default member)
						send_by = id # OR username OR id_or_username
						notification_username = joshuabza # can be public username (bot/user/group/channel) or invite link (to join first before posting) - should have send message permission
						job_name = cryptosnack mass mail
						max_send_limit = 100 # free trial
						active = True # if off, no user will send


						[a4123]
						# group id's which this session added to job - ie. is already a member of
						groups_added = 5225368
							124124124
							124124124
							412412412 
						proc_id = 

						[snacks6421]
						proc_id = 21412

				"""

				# log.info("Sent job to subprocess.")

				return True

			except Exception as e:
				log.error(e)

		else:
			log.error(f"File '{DIR['BASE']+'/'+str(file)}' does not exist in Working Directory.")
		
		return False

	@log.catch()
	def SendToSubprocess(self,file, session = '', **kwargs ):
		"""	
			file = filename located in BASE directory, with extension (.py)
			session = name of session to be used. 

			accepts kwargs:
				job = str
				args = list (additional arguments for command line, eg. --debug --simulation --no-wait)
				config = dict (additional configuration for jobs)
		"""

		if os.path.isfile(DIR['BASE']+'/'+str(file)):

			try:
				if isinstance(session,str):
					session = [session]
				
				args1 = [sys.executable, DIR['BASE']+'/'+file]
				args2 = []
				args3 = []
				job = ''
				job_config_dict = {}
				section = 'Common'
				restart = False

				for a in kwargs.keys():
					if a == 'job':
						args2.append(kwargs[a])
						job = str(kwargs[a])
						section = job
					if a == 'args':
						if isinstance(kwargs[a], list):
							args3 = [*args3,*kwargs[a]]
						else:
							args3.append(kwargs[a])
					if a == 'config':
						if isinstance(kwargs[a], dict):
							job_config_dict = kwargs[a]						
					if a == 'restart':
						restart = bool(kwargs[a])

				subprocess_config = configparser.ConfigParser(allow_no_value=True)
				subprocess_config.read(DIR['SUBPROCESSES']+'/'+str(os.path.splitext(file)[0])+'.txt')

				log.info("Sending to Subprocess: "+str([*args1,session,*args2,*args3]))

				"""
					Subprocess file examples....
						config_client.txt
							[Common]
								a4123 = 41241
								snacks6421 = 67853
								snacks5311 = 77839

						send_message.txt
							[cryptosnacks]
								a4123 = 12412
								snacks6421 = 67853
								snacks5311 = 77839
							
							[nftbabes]
								a4123 = 41241
								snacks6421 = 67853
								snacks5311 = 77839
				"""
			except Exception as e:
				log.error(e)

			try:
				for s in session:
					# print([*args1,s,*args2,*args3])

					# check if subprocess already setup:
					old_pid = subprocess_config.get(section,s,fallback='')

					if old_pid:
						# process exists. Need to make sure it is closed.
						p = None
						try:
							p = psutil.Process(int(old_pid))
							if restart:
								p.terminate()
							else: 
								# currently running and dont want to restart.
								log.info(f"Subprocess {os.path.splitext(file)[0]}[{section}]:{s} (pid:{int(old_pid)}) already running and restart=False. Skipping.")
								continue

						except psutil.NoSuchProcess:
							pass
						except Exception as e:
							log.error(e)
						try:
							if p:
								gone, alive = psutil.wait_procs(p, timeout=3)
								for p2 in alive:
									p2.kill()
						except Exception as e:
							log.error(e)

					# ready to run new subprocess
					try:
						proc = subprocess.Popen([*args1,s,*args2,*args3],
				                stdout=subprocess.PIPE, 
				                stderr=subprocess.STDOUT)

						if not subprocess_config.has_section(section):
							subprocess_config.add_section(section)
						subprocess_config[section][s] = str(proc.pid)
						log.info(f"Subprocess {os.path.splitext(file)[0]}[{section}]:{s} started with process ID: {proc.pid}")

					except Exception as e:
						log.error(e)
					

			except Exception as e:
				log.error(e)

			try:
				# save config
				with open(DIR['SUBPROCESSES']+'/'+str(os.path.splitext(file)[0])+'.txt', 'w', encoding='utf-8') as configfile:
					subprocess_config.write(configfile)

				"""
					Subprocess Job config file
					eg.
						[Common]
						target_users = admin # OR member OR all (default member)
						send_by = id # OR username OR id_or_username
						notification_username = joshuabza # can be public username (bot/user/group/channel) or invite link (to join first before posting) - should have send message permission
						job_name = cryptosnack mass mail
						max_send_limit = 100 # free trial
						active = True # if off, no user will send


						[a4123]
						# group id's which this session added to job - ie. is already a member of
						groups_added = 5225368
							124124124
							124124124
							412412412 
						proc_id = 

						[snacks6421]
						proc_id = 21412

				"""
				if job:
					if not os.path.isdir(DIR['SUBPROCESSES']+'/'+str(job)):
						mkdir(DIR['SUBPROCESSES']+'/'+str(job))
					# job_config = configparser.ConfigParser({'groups_added':'','proc_id':''},allow_no_value=True)
					# job_config.read(DIR['SUBPROCESSES']+'/'+str(job)+'/config.ini')
					# if len(job_config_dict)>0:
					# 	job_config.read_dict(job_config_dict)
					# with open(DIR['SUBPROCESSES']+'/'+str(job)+'/config.ini', 'w', encoding='utf-8') as f:
					# 	job_config.write(f)

				log.info("Sent job to subprocess.")

				return True

			except Exception as e:
				log.error(e)

		else:
			log.error(f"File '{DIR['BASE']+'/'+str(file)}' does not exist in Working Directory.")
		
		return False

# if __name__ == 'lib.core':
# only display if called from base directory
log.debug("__name__ = "+str(__name__))
clear()
# print(banner+"\n")
_interval = str(time.time() - _startup_time)
log.debug("Time to load header: "+_interval[:_interval.rfind(".")+3] + " seconds")

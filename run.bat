@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

ECHO.
ECHO Telegram Tools
ECHO.

where.exe python > NUL

IF ERRORLEVEL 1 (
   ECHO Python is not installed
   EXIT 1
)

cd /D "%~dp0"

IF NOT EXIST venv (
    ECHO Creating virtual environment
    python -m venv venv

    CALL venv/Scripts/activate.bat

    ECHO Installing dependencies
    python TelegramTools.py --install
) else (
    ECHO Activating virtual environment
    CALL venv/Scripts/activate.bat
)

ECHO %*
SET _all=%*
CALL SET args=%%_all:*%2=%%
SET args=%2%args%

IF "%1" == "--cron" (
    ECHO Running Cron with args: %args%
    python Cron.py %args%
) ELSE (
    ECHO Running Telegram Tools with args: $@
    python TelegramTools.py %*
)
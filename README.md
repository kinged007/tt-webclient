# Telegram Tools (lite)
## Client Manager 

Client/session manager for Telegram. 
Sessions/clients (ie. Telegram accounts) are stored in `data/telethon_sessions`. 
Session files need to have a corresponding JSON metadata file which has the same name.
If JSON file is missing, you can use the tool available to create missing JSON metdata files.

Web Client opens the telegram web client in the Chrome browser. You may need to keep `run` file open while you work in the browser client.

## Getting started

Requires Python 3.6+ to be installed in your system. We recommend using the latest versions of both Python 3 and pip.

Get Python 3 from https://www.python.org/downloads/ (or with your package manager).

For Windows, Microsoft Visual C++ 14.0 or greater is required. Download through Build Tools and install the Workload "Desktop Development with C++":
https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16

Please make sure PIP is also installed.
See https://pip.pypa.io/en/stable/installation/

All other dependecies will be downloaded automatically on first fun.

## How to download
![how_to_download](/uploads/638aca9edf05fa3ae219bac5f2db4f2c/how_to_download.jpg)

## How to run
Download the ZIP file, extract it. Then run it.

### Linux/MacOS
```
sh run.sh
```
Run `sh run.sh --help` to see runtime options.

### Windows
```
run.bat
```

Run `run.bat --help` to see runtime options.

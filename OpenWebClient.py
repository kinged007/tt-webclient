from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from seleniumwire import webdriver

from lib.TelethonClient import TelethonClient
from lib.core import *


class OpenWebClient(TelegramTools):

    @log.catch()
    def __init__(self):
        log.info(sys.argv)
        self.Require('session')  # required at CLI level, therefore stored in self container

        super().__init__()
        # self._session = session

        _start = time.time()

        self.metadata = self.GetSessionMetadata(self.SessionName)

        if self.metadata:
            # self.loop = asyncio.get_event_loop()
            # self.loop.run_until_complete(self.start())
            asyncio.run(self.start())
        else:
            log.error("Metadata is required.")

        log.debug("Elapsed time to complete task: " + str(time.time() - _start) + " seconds")

    @log.catch()
    def get_proxy_options(self):

        if self.metadata['proxy']:
            #     "proxy": [2, "154.16.243.235", 6379, true, "lkywtyij", "co1w2qn3lu7j"],
            # selenium-wire proxy settings
            m = self.metadata['proxy']
            if self.metadata['proxy'][4]:
                options = {
                    'proxy': {
                        'http': 'socks5://{}:{}@{}:{}'.format(m[4], m[5], m[1], m[2]),
                        'https': 'socks5://{}:{}@{}:{}'.format(m[4], m[5], m[1], m[2]),
                        'no_proxy': 'localhost,127.0.0.1'  # excludes
                    }
                }
            else:
                options = {
                    'proxy': {
                        'http': 'socks5://{}:{}'.format(m[1], m[2]),
                        'https': 'socks5://{}:{}'.format(m[1], m[2]),
                        'no_proxy': 'localhost,127.0.0.1'  # excludes
                    }
                }

            return options

        return None

    @log.catch()
    async def start(self):

        # # # use authenticated proxy
        # while True:
        # 	if 'username' not in self.Config['proxy']:
        # 		self.GetProxy()
        # 		continue
        # 	else:
        # 		break

        # chromedrive path
        if platform == 'win32':
            executable_path = {'executable_path': self.Path(DIR['BASE'],"drivers","chromedriver.exe")}
        else:
            executable_path = {'executable_path': self.Path(DIR['BASE'],"drivers","chromedriver")}

        # other chrome options
        chrome_options = webdriver.ChromeOptions()
        # if not '--no-headless' in sys.argv:
        # 	chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--incognito')
        chrome_options.add_argument('--ignore-certificate-errors-spki-list')
        chrome_options.add_argument('--ignore-ssl-errors')
        chrome_options.add_argument('--ignore-certificate-errors')
        # chrome_options.add_experimental_option("prefs", {"profile.managed_default_content_settings.images": 2})
        chrome_options.add_experimental_option("detach", True)
        capabilities = DesiredCapabilities.CHROME.copy()
        capabilities['acceptInsecureCerts'] = True

        browser = webdriver.Chrome(executable_path['executable_path'], options=chrome_options,
                                   seleniumwire_options=self.get_proxy_options(), desired_capabilities=capabilities)
        # browser = Browser(executable_path['executable_path'], options=chrome_options,seleniumwire_options=self.get_proxy_options(), desired_capabilities=capabilities)

        AcceptedResponseCodes = [200,
                                 302]  # 302 should be accepted if we are not loading images (resources are redirected away)

        # URL = "https://www.whatismyip.com/"
        # URL = "http://my.telegram.org/"
        URL = "https://web.telegram.org/k/"

        _failed_attempts = 5

        while True:
            if _failed_attempts <= 0:
                log.error("Could not connect multiple times.")
                browser.quit()
                await self.client.Disconnect()
                return
            # try to connect
            try:
                browser.get(URL)
                browser.implicitly_wait(5)  # wait a bit
                # time.sleep(10)
                if not browser.title == 'Telegram Web':
                    print(browser.title)
                    time.sleep(3)
                    continue
                # should have the right page
                break

            except Exception as e:
                log.error(e)

                _failed_attempts -= 1
                continue

        print("Logging in to Telegram...")
        self.client = TelethonClient(self.SessionName)

        if not await self.client.ConnectClient():
            log.error("Could not connect to client.")
            return

        while True:
            try:
                # await async_wait(2)
                ph = browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[1]/div/div[3]/div[2]/div[1]')
                break
            except:
                try:
                    browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[2]/div/div[2]/button').click()
                    continue
                except Exception as e:
                    log.error(e)
                    browser.quit()
                    await self.client.Disconnect()
                    return

        # assuming got a good response. continue

        try:
            await async_wait(2)
            ph.clear()
            await async_wait(1)
            ph.send_keys('+' + str(self.metadata.get('phone')))
            button = browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[1]/div/div[3]/button[1]')
            button.click()

        except Exception as e:
            log.error(e)
            browser.quit()
            await self.client.Disconnect()
            return

        print("Waiting for Login code.")
        # print("looking for"+str(self.Session['me'].phone)[-4:] )
        # # Wait until password box appears
        # print("in "+browser.find_element(By.CLASS_NAME, 'phone').text)
        # WebDriverWait(browser, timeout=10).until(lambda d: str(self.Session['me'].phone)[-4:] in d.find_element(By.CLASS_NAME, 'phone').text  )
        browser.implicitly_wait(10)  # wait a bit

        # enter password
        code = ''
        while not code:
            # getting code
            print("Getting confirmation code...")
            await async_wait(3)
            code = await self.client.GetConfirmationCode()
            pass

        log.info("Telegram Web Login Authentication code received: " + code)

        try:
            browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[3]/div/div[3]/div/input').send_keys(
                code)

        except Exception as e:
            log.error(e)
            await self.client.Disconnect()
            browser.quit()

        await async_wait(5)

        try:
            c = browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[4]').get_attribute("class")
            if 'active' in c:
                # expecting password
                print("2FA is Enabled...")
                # try:
                # 	from easygui import msgbox
                # 	msgbox("If it doesn't work automatically, you can use this password:\n"+self.metadata.get('twoFA'), title="2FA Password is required")
                # except Exception as e:
                # 	log.error(e)

                browser.find_element(By.XPATH,
                                     '//*[@id="auth-pages"]/div/div[2]/div[4]/div/div[2]/div/input[2]').send_keys(
                    str(self.metadata.get('twoFA')))
                browser.find_element(By.XPATH, '//*[@id="auth-pages"]/div/div[2]/div[4]/div/div[2]/button').click()
        except:
            pass

        # WebDriverWait(browser, timeout=10).until(lambda d: d.find_element(By.ID, 'page-chats')  )

        print("logged in!")

        # try:
        # 	browser.find_element(By.ID, 'page-chats')
        # except:
        # 	log.error("Could not find page chats.")
        # 	await self.Client.Disconnect()
        # 	browser.quit()
        # 	return

        await self.client.Disconnect()

        while True:
            try:
                await async_wait(30)
                browser.title
            except:
                # closed
                browser.quit()
                break

        return

        # browser.implicitly_wait(100)

        # print(f"Page title: {browser.title}")

        # try:
        # 	login_form = browser.find_element(By.ID, "my_send_form")
        # except:
        # 	# form cannot be found
        # 	print("Cant find login form.")
        # 	browser.quit()
        # 	# self.Client.disconnect()
        # 	return

        # # logout if logged in
        # try:
        # 	browser.find_element(By.LINK_TEXT, 'Log out').click()
        # 	print("Browser had a session. Logging out.")
        # 	time.sleep(3)
        # 	browser.get('https://my.telegram.org/auth')
        # except:
        # 	pass

        # # login
        # try:
        # 	login_form.find_element(By.ID,'my_login_phone').send_keys('+'+str(self.Session['me'].phone))
        # 	login_form.find_element(By.XPATH, '//*[@id="my_send_form"]/div[2]/button').click()
        # 	print("Waiting for Login code.")
        # except:
        # 	print("Could not find Phone input field.")

        # # Wait until password box appears
        # WebDriverWait(browser, timeout=10).until(lambda d: d.find_element(By.ID, 'my_password') )

        # # enter password
        # code = ''
        # while not code:
        # 	# getting code
        # 	code = self.loop.run_until_complete(self.GetConfirmationCode())
        # 	pass

        # log.info("Telegram Web Login Authentication code received: "+code)

        # # free up database
        # self.Client.disconnect()

        # try:
        # 	time.sleep(5)
        # 	browser.implicitly_wait(100) # wait a bit
        # 	browser.find_element(By.ID, 'my_password').send_keys(code)
        # 	time.sleep(1)
        # 	browser.find_element(By.XPATH, '//*[@id="my_login_form"]/div[4]/button').click() # click sign in
        # except:
        # 	log.error("Could not locate password and sign in elements.")
        # 	browser.quit()
        # 	# self.Client.disconnect()
        # 	return

        # browser.implicitly_wait(10) # wait a bit
        # time.sleep(4)
        # browser.get('https://my.telegram.org/apps')

        # try:
        # 	browser.find_element(By.ID, 'app_create_form')
        # 	# no app exists
        # 	title = "mytgdapp"+str(random.randrange(100,999))
        # 	browser.find_element(By.ID, 'app_title').send_keys(title)
        # 	browser.find_element(By.ID, 'app_shortname').send_keys(title.replace(" ","").lower())
        # 	browser.find_element(By.XPATH, '//*[@id="app_create_form"]/div[4]/div/div[5]/label').click()
        # 	time.sleep(1)
        # 	browser.find_element(By.ID, 'app_save_btn').click()

        # except Exception as e:
        # 	log.error(e)
        # 	# app already exists
        # 	pass

        # time.sleep(100)

        # api_id = api_hash = ''

        # try:
        # 	# account must already have an app configured
        # 	api_id = browser.find_element(By.XPATH, '//*[@id="app_edit_form"]/div[1]/div[1]/span/strong').text
        # 	api_hash =  browser.find_element(By.XPATH, '//*[@id="app_edit_form"]/div[2]/div[1]/span').text
        # except Exception as e:
        # 	log.error(e)

        # if api_id and api_hash:
        # 	log.info(f"Telegram API Credentials: ID={api_id} | Hash={api_hash}")
        # 	# update database
        # 	self.loop.run_until_complete(self.UpdateSessionToDb(api_id=api_id, api_hash=api_hash))
        # else:
        # 	log.error("Could not find API credentials. Check website has not changed and update code.")

        # # print(browser.html)
        # time.sleep(100)
        # browser.quit()

        # log.debug("Elapsed time to complete task: "+str(time.time()-_start)+" seconds")
        # # self.Client.disconnect()
        # return

        return


if __name__ == "__main__":
    a = OpenWebClient()

    sys.exit(0)

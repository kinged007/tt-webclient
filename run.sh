#!/bin/bash

echo
echo Telegram Tools
echo

if ! command -v python3 &> /dev/null;
then
    echo Python is not installed
    exit 1
fi

cd "$(dirname "$0")" || exit

if [ ! -d "venv" ];
then
    echo Creating virtual environment
    python3 -m venv venv

    . venv/bin/activate

    echo Installing dependencies
    python3 TelegramTools.py --install
else
    echo Activating virtual environment
    . venv/bin/activate
fi


if [ "$1" = "--cron" ];
then
    shift
    echo Running Cron with args: "$@"
    python3 Cron.py "$@"
else
    echo Running Telegram Tools with args: "$@"
    python3 TelegramTools.py "$@"
fi